﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerShootingBot.Common
{
    public interface ILog
    {
        void WriteLine(string msg, LogImportance importance = LogImportance.Normal);
    }
}
