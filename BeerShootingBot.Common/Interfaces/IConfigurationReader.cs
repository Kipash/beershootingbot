﻿namespace BeerShootingBot.Common
{
    public interface IConfigurationReader
    {
        AppConfiguration ReadConfiguration();

        IPlugin[] Plugins { get; set; }

        GameStatus Game { get; set; }
    }
}
