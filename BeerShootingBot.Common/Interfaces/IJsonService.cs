﻿using System.Net;

namespace BeerShootingBot.Common
{
    public delegate void JsonServiceDelegate<T>(string address, T results);

    public interface IJsonService<T>
    {
        event JsonServiceDelegate<T> DataRetrieved;

        void Enqueue(string address);

        void ClearQueue();
    }
}
