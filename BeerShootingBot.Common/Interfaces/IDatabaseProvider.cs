﻿
using System.Data.Common;

namespace BeerShootingBot.Common
{
    public interface IDatabaseProvider
    {
        DbProviderFactory GetFactory();

        /// <summary>
        /// Return sponky bot database structure with this bots extra tables
        /// </summary>
        /// <returns></returns>
        string[] GetDatabaseCreationScript();
    }
}
