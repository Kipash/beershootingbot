﻿using System.Collections.Generic;

namespace BeerShootingBot.Common
{
    public interface IPlugin
    {
        string Name { get; }
        string Version { get; }

        bool IsEnabled { get; }

        /// <summary>
        /// List of commands used
        /// </summary>
        /// <returns></returns>
        Dictionary<string, Command> Commands { get; }
    }
}
