﻿using System.Collections.Generic;

namespace BeerShootingBot.Common
{
    public delegate void PlayerRetrievedDelegate(Player[] onlinePlayers);

    public interface IRconManager
    {
        event PlayerRetrievedDelegate PlayerRetrieved;

        void QueueCommand(string command);
        void BatchCommands(IEnumerable<string> commands);
        void BigText(string msg);
        void MessageToPlayer(int playerSlot, string msg);
        void ClearQueue();
    }
}
