﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerShootingBot.Common
{
    public delegate void UnknownEventDelegate(string text);
    public delegate void CommandReceivedDelegate(Player sender, string text);
    public delegate void PlayerEnteredDelegate(Player newPlayer);
    public delegate void PlayerLeftDelegate(Player exitedPlayer);
    public delegate void PlayerTeamChangedDelegate(Player newData, GameTeam previousTeam);
    public delegate void PluginChangedDelegate(string pluginName, bool enabled);
    public delegate void PlayerKilledDelegate(Player killer, Player killed, KilledBy via, HitLocations location);
    public delegate void PlayerHitDelegate(Player shooter, Player victim, HitItem used, HitLocations location);

    // Administration only events
    public delegate Player PlayerDataNeededDelegate(Player newPlayer);

    public interface IEventNotifier
    {
        /// <summary>
        /// Command called from administration plugin only
        /// </summary>
        /// <param name="pluginName"></param>
        /// <param name="enabled"></param>
        void ChangePlugin(string pluginName, bool enabled);

        /// <summary>
        /// Event fired when a player enters the server and info is received, also when map is started
        /// </summary>
        event PlayerEnteredDelegate PlayerEntered;

        event PlayerDataNeededDelegate PlayerDataNeeded;

        /// <summary>
        /// Event fired when a player leaves the server and info is received, also when map is started
        /// </summary>
        event PlayerLeftDelegate PlayerLeft;

        /// <summary>
        /// Event fired when a player tean is changed
        /// </summary>
        event PlayerTeamChangedDelegate PlayerTeamChanged;

        /// <summary>
        /// Event fired when a player is killed
        /// </summary>
        event PlayerKilledDelegate PlayerKilled;

        /// <summary>
        /// Event fired when a player is killed
        /// </summary>
        event PlayerHitDelegate PlayerHit;

        /// <summary>
        /// Event fired on map started/restarted
        /// </summary>
        event UnknownEventDelegate MapChanged;

        /// <summary>
        /// Event fired when a command is received, plugins will process it
        /// </summary>
        event CommandReceivedDelegate CommandReceived;

        /// <summary>
        /// All plugins must subscribe to this event
        /// </summary>
        event PluginChangedDelegate PluginChanged;

        void ReadInitialStatus();
    }
}
