﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BeerShootingBot.Common
{
    public abstract class PluginBase : IPlugin
    {
        // Initialized using GetCommands() function
        public Dictionary<string, Command> Commands { get; private set; }

        protected bool currentEnabled;
        protected IRconManager rcon;
        protected IConfigurationReader configReader;
        protected AppConfiguration config;
        protected IEventNotifier eventNotifier;
        protected DataBase db;

        public bool IsEnabled { get { return currentEnabled; } }

        public PluginBase(IConfigurationReader configReader, IEventNotifier eventNotifier, IRconManager rconManager, IDatabaseProvider databaseProvider)
        {
            rcon = rconManager;
            this.eventNotifier = eventNotifier;
            this.configReader = configReader;

            Commands = GetCommands();

            config = this.configReader.ReadConfiguration();

            db = new DataBase(config.Database.ProviderName, config.Database.ConnectionString);

            currentEnabled = GetEnabled(config);

            this.eventNotifier.CommandReceived += EventNotifier_CommandReceived;
        }

        protected abstract bool GetEnabled(AppConfiguration config);

        protected abstract Dictionary<string, Command> GetCommands();

        private void EventNotifier_CommandReceived(Player sender, string text)
        {
            if (!currentEnabled) return;

            CommandOutput outputType = (text.StartsWith('!')) ? CommandOutput.PrivateMessage : CommandOutput.BigText;
            text = text.Substring(1);
            string[] commandParts = text.Split(' ');
            string commandTyped = commandParts[0];
            string[] commandArguments = commandParts.Skip(1).ToArray();

            //Check if the command is registered to this plugin
            var pairFound = Commands.SingleOrDefault(p => p.Key.Equals(commandTyped, StringComparison.CurrentCultureIgnoreCase) || (!string.IsNullOrEmpty(p.Value.Alias) && p.Value.Alias.Equals(commandTyped, StringComparison.CurrentCultureIgnoreCase)));
            if (pairFound.Equals(default(KeyValuePair<string, Command>))) return;

            Command commandToExecute = pairFound.Value;

            // Look sender level and command level, if command authorized
            if ((int)sender.AdminLevel < (int)commandToExecute.MinimunLevel)
            {
                // Log wrong command executed
                return;
            }

            try
            {
                string feedback = "Error: no feedback - no callback was found";
                if (commandToExecute.Execute != null)
                    feedback = commandToExecute.Execute(sender, commandArguments);

                if (!string.IsNullOrEmpty(feedback))
                {
                    // Dont allow non-privileged users to spam with bigtext
                    if (outputType == CommandOutput.BigText && (int)sender.AdminLevel < (int)AdminLevel.Moderator) outputType = CommandOutput.PrivateMessage;

                    switch (outputType)
                    {
                        case CommandOutput.PrivateMessage:
                            rcon.MessageToPlayer(sender.ServerSlot.Value, feedback);
                            break;
                        case CommandOutput.BigText:
                            rcon.BigText(feedback);
                            break;
                    }
                }
            }
            catch(Exception exc)
            {
                //TODO: add logger?
                Console.WriteLine("Exception executing a command: " + exc.Message);
            }
        }

        public abstract string Name { get; }

        public abstract string Version { get; }

        protected Player LookupPlayer(int senderSlot, string searchTerm)
        {
            IDbCommand command = db.GetCommand();
            Player returned = null;
            Func<IDataReader, Player> getPlayerData = p =>
            {
                return new Player()
                {
                    AdminLevel = p["registeredId"] == DBNull.Value ? AdminLevel.Guest : (AdminLevel)int.Parse(p["admin_role"].ToString()),
                    Guid = (string)p["guid"],
                    IP = (string)p["ip_address"],
                    IsBot = false,
                    Name = (string)p["name"],
                    RegisteredId = p["registeredId"] == DBNull.Value ? null : (long?)p["registeredId"],
                    UserId = (long)p["userId"]
                };
            };

            if (searchTerm.StartsWith('@'))
            {
                if (long.TryParse(searchTerm.Substring(1), out long userId))
                {
                    IDataReader readerId = command
                         .Text("select p.id as userId, p.name, p.guid, p.ip_address, x.id as registeredId, x.admin_role from player p left join xlrstats x on p.guid = x.guid where p.id = @id")
                         .AddParameter("@id", userId)
                         .ExecuteReader();
                    if (readerId.Read())
                    {
                        returned = getPlayerData(readerId);
                    }
                    readerId.Close();
                }
                return returned;
            }
            List<Player> foundPlayers = new List<Player>();
            IDataReader readerName = command
                .Text("select p.id as userId, p.name, p.guid, p.ip_address, x.id as registeredId, x.admin_role from player p left join xlrstats x on p.guid = x.guid where p.name like @name")
                .AddParameter("@name", '%' + searchTerm + '%')
                .ExecuteReader();
            while (readerName.Read())
            {
                foundPlayers.Add(getPlayerData(readerName));
            }
            readerName.Close();
            switch(foundPlayers.Count)
            {
                case 0:
                    return null;
                case 1:
                    return foundPlayers[0];
                default:
                    foreach(Player found in foundPlayers)
                    {
                        rcon.MessageToPlayer(senderSlot, $"Player found (id,name,group):{found.UserId.Value},{found.Name},{found.AdminLevel.GetDescription()}");
                    }
                    return null;
            }
        }

        protected void ForcePlayer(Player playerToMove, GameTeam newTeam)
        {
            rcon.QueueCommand($"forceteam {playerToMove.ServerSlot.Value} {newTeam}");
        }

        protected Player[] GetOnlinePlayers(Func<Player, bool> filter = null)
        {
            IEnumerable<Player> returned = configReader.Game.Players.Where(p => p != null);
            if (filter == null) return returned.ToArray();
            return returned.Where(filter).ToArray();
        }

        protected void MassivePrivateMessage(AdminLevel minimumLevel, string message)
        {
            Player[] playersToPM = GetOnlinePlayers(p => p.AdminLevel >= minimumLevel);
            foreach(Player playerToPM in playersToPM)
            {
                rcon.MessageToPlayer(playerToPM.ServerSlot.Value, message);
            }
        }
    }

    public enum CommandOutput
    {
        PrivateMessage,
        BigText
    }
}
