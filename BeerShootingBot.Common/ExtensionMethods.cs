﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace BeerShootingBot.Common
{
    public static class ExtensionMethodsBSBCommon
    {
        private static Dictionary<string, string> cachedDescriptions = new Dictionary<string, string>();
        private static Dictionary<string, bool> cachedHeadshoots = new Dictionary<string, bool>();
        private static Dictionary<string, HitItem> cachedHitItems = new Dictionary<string, HitItem>();
        private static Dictionary<string, string> cachedHitNames = new Dictionary<string, string>();

        public static string GetDescription(this HitLocations enumValue)
        {
            return GetAttributeData<HitLocations, string, HitLocationAttribute>(enumValue, cachedDescriptions, p => p.Name);
        }

        public static string GetDescription(this GameTeam enumValue)
        {
            return GetAttributeData<GameTeam, string, DescriptionAttribute>(enumValue, cachedDescriptions, p => p.Description);
        }

        public static string GetDescription(this AdminLevel enumValue)
        {
            return GetAttributeData<AdminLevel, string, DescriptionAttribute>(enumValue, cachedDescriptions, p => p.Description);
        }

        public static bool IsHeadshoot(this HitLocations enumValue)
        {
            return GetAttributeData<HitLocations, bool, HitLocationAttribute>(enumValue, cachedHeadshoots, p => p.IsHeadshoot);
        }

        public static bool HasHitAssociated(this KilledBy enumValue)
        {
            return GetAttributeData<KilledBy, HitItem, KilledByAttribute>(enumValue, cachedHitItems, p => p.RegisteredHit) != HitItem.None;
        }
        public static string GetName(this KilledBy enumValue)
        {
            return GetAttributeData<KilledBy, string, KilledByAttribute>(enumValue, cachedHitNames, p => p.Name);
        }

        public static HitItem GetHitItem(this KilledBy enumValue)
        {
            return GetAttributeData<KilledBy, HitItem, KilledByAttribute>(enumValue, cachedHitItems, p => p.RegisteredHit);
        }

        private static ReturnedType GetAttributeData<EnumType, ReturnedType, AttributeType>(EnumType enumValue, Dictionary<string, ReturnedType> cache, Func<AttributeType, ReturnedType> accesor)
        {
            string value = enumValue.ToString();
            if (cache.ContainsKey(value)) return cache[value];

            // Not cached, use reflection and cache the value (reflection is poor performance)
            Type type = enumValue.GetType();
            var memberInfo = type.GetMember(value);
            object[] attributes = memberInfo[0].GetCustomAttributes(typeof(AttributeType), false);
            ReturnedType returned = default(ReturnedType);
            if (attributes.Length > 0) returned = accesor((AttributeType)attributes[0]);
            cache.Add(value, returned);
            return returned;
        }
    }
}

namespace System.IO
{
    public static class SystemIOExtensions
    {
        public static T AsJson<T>(this FileInfo file)
        {
            string content = File.ReadAllText(file.FullName);
            T returned = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(content);
            return returned;
        }
    }
}

