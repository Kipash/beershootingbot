﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;

namespace BeerShootingBot.Common
{
    public class DataBase
    {
        private DbProviderFactory factory;
        private string connectionString;

        public DataBase(string providerName, string connectionString)
        {
            this.connectionString = connectionString;
            this.factory = GetFactory(providerName);
        }

        private IDbConnection FetchConnection()
        {
            IDbConnection returned = factory.CreateConnection();
            returned.ConnectionString = connectionString;
            return returned;
        }

        private DbProviderFactory GetFactory(string providerName)
        {
            // .NET core 2 does not implement DbProviderFactories.GetFactory(string providerName).
            // it's support starts in 3.0 and the IDE to 3.0 is VS 2019...
            
            switch (providerName)
            {
                case "System.Data.SQLite":
                    return System.Data.SQLite.SQLiteFactory.Instance;
                default:
                    throw new NotImplementedException("No support to that database");
            }
        }

        public IDbCommand GetCommand()
        {
            IDbConnection connection = FetchConnection();
            IDbCommand returned = connection.CreateCommand();
            return returned;
        }
        
        public IDbDataAdapter GetDataAdapter()
        {
            // Not all providers have DataAdapter classes, for example Microsoft.Data.SQLite does not have them
            DbDataAdapter returned = factory.CreateDataAdapter();
            return returned;
        }
    }

    public static class FluentIDbCommand
    {
        public static IDbCommand Text(this IDbCommand command, string text)
        {
            if (command.Connection.State != ConnectionState.Open)
            {
                command.Connection.Open();
            }
            command.Parameters.Clear();
            command.CommandType = CommandType.Text;
            command.CommandText = text;
            return command;
        }

        public static IDbCommand AddParameter(this IDbCommand command, string name, string value)
        {
            IDbDataParameter parameter = command.CreateParameter();
            parameter.ParameterName = name;
            parameter.DbType = DbType.String;
            command.Parameters.Add(parameter);
            if (value == null)
            {
                parameter.Value = DBNull.Value;
            }
            else
            {
                parameter.Value = value;
            }
            return command;
        }

        /*
        public static IDbCommand AddParameter(this IDbCommand command, string name, int value)
        {
            IDbDataParameter parameter = command.CreateParameter();
            parameter.ParameterName = name;
            parameter.DbType = DbType.Int32;
            command.Parameters.Add(parameter);
            parameter.Value = value;
            return command;
        }*/

        public static IDbCommand AddParameter(this IDbCommand command, string name, long value)
        {
            IDbDataParameter parameter = command.CreateParameter();
            parameter.ParameterName = name;
            parameter.DbType = DbType.Decimal;
            command.Parameters.Add(parameter);
            parameter.Value = value;
            return command;
        }

        public static IDbCommand AddParameter(this IDbCommand command, string name, DateTime value)
        {
            IDbDataParameter parametro = command.CreateParameter();
            parametro.ParameterName = name;
            parametro.DbType = DbType.DateTime;
            command.Parameters.Add(parametro);
            parametro.Value = value;
            return command;
        }
    }
}
