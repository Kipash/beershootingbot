﻿using System;
using System.Collections.Generic;

namespace BeerShootingBot.Common
{
    public class FakeRcon : IRconManager
    {
        private AppConfiguration config;

        public event PlayerRetrievedDelegate PlayerRetrieved;

        public FakeRcon(IConfigurationReader configReader)
        {
            config = configReader.ReadConfiguration();
        }

        public void BatchCommands(IEnumerable<string> commands)
        {
            foreach(string command in commands) Console.WriteLine($"Command: {command}");
        }

        public void BigText(string msg)
        {
            Console.WriteLine($"Bigtext: {msg}");
        }

        public void MessageToPlayer(int playerSlot, string msg)
        {
            Console.WriteLine($"[PM to {playerSlot}]: {msg}");
        }

        public void QueueCommand(string command)
        {
            Console.WriteLine($"Command: {command}");
        }

        public void ClearQueue()
        {

        }
    }
}
