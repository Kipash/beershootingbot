﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BeerShootingBot.Common
{
    public class VpnDetectionService : JsonServiceBase<bool>
    {
        // 1000 / day -> 41.6 / hour ->  every 1.5 minutes
        protected override TimeSpan MinimunFrecuency => new TimeSpan(0, 1, 30);

        public VpnDetectionService(ILog logger) : base(logger)
        {

        }

        protected override bool ConsumeService(string address)
        {
            string url = $"https://ip.teoh.io/api/vpn/{address}";
            Task<VpnDetectionJson> task = GetExternalJson<VpnDetectionJson>(url);
            task.Wait();
            VpnDetectionJson returned = task.Result;
            return returned.vpn_or_proxy != null && returned.vpn_or_proxy.Equals("yes", StringComparison.CurrentCultureIgnoreCase);
        }

        public class VpnDetectionJson
        {
            public string ip { get; set; }
            public string organization { get; set; }
            public string asn { get; set; }
            public string type { get; set; }
            public string risk { get; set; }
            public int is_hosting { get; set; }
            public string vpn_or_proxy { get; set; }
        }
    }
}
