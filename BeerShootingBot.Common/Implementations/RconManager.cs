﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace BeerShootingBot.Common
{
    public class RconManager : IRconManager
    {
        private Queue<string> rconQueue = new Queue<string>();

        private string password;
        private IPAddress serverIP;
        private int port;
        private ILog logger;
        private int rconDelay;

        private Socket socket;

        public event PlayerRetrievedDelegate PlayerRetrieved;

        public RconManager(IConfigurationReader configReader, ILog logger)
        {
            AppConfiguration config = configReader.ReadConfiguration();
            password = config.Rcon.Password;
            serverIP = IPAddress.Parse(config.Rcon.IP);
            port = config.Rcon.RconPort;
            rconDelay = config.Rcon.Delay;
            this.logger = logger;

            socket = Connect();
            while (!socket.Connected) { Console.WriteLine("Waiting for connection"); }
            
            TestConnection(socket);

            // Start backgroud periodic executions
            //StatusLoopAsync();
            QueueLoopAsync(socket);
        }

        public void BigText(string msg)
        {
            QueueCommand($"bigtext \"{msg}\"");
        }

        private Socket Connect()
        {
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            try
            {
                //attempts to connect
                socket.Connect(serverIP, port);
                logger.WriteLine($"Connecting to server ({serverIP}:{port})");
            }
            catch (Exception e)
            {
                //connect failed
                logger.WriteLine($"<Error> Can't connect to server({serverIP}:{port})\nError: {e.Message}", LogImportance.Error);
                return null;
            }

            return socket;
        }

        public void MessageToPlayer(int playerSlot, string msg)
        {
            QueueCommand($"tell {playerSlot} {msg}");
        }

        public void QueueCommand(string command)
        {
            logger.WriteLine($"rcon: {command}");
            rconQueue.Enqueue(command);
        }

        public void BatchCommands(IEnumerable<string> commands)
        {
            foreach (string command in commands)
            {
                Send(socket, $"rcon {password} {command}");
            }
        }

        /*
        private async Task StatusLoopAsync()
        {
            while (true)
            {
                await Task.Delay(10000);
                bool playersFetched = FetchPlayers();
            }
        }*/

        private async Task QueueLoopAsync(Socket socket)
        {
            while (true)
            {
                await Task.Delay(rconDelay);
                if (rconQueue.Count > 0)
                {
                    var response = Send(socket, $"rcon {password} {rconQueue.Dequeue()}");
                    //PrintResponse(response);
                }
            }
        }

        private bool TestConnection(Socket socket)
        {
            try
            {
                logger.WriteLine("Testing the connection");
                bool isValid = CheckResponse(Send(socket, $"rcon {password} g_gravity"));
                return isValid;
            }
            catch (Exception e)
            {
                logger.WriteLine($"<Error> can't connect to the server. Wrong ip/port? ; full message: {e.Message}", LogImportance.Error);
                return false;
            }
        }

        private bool CheckResponse(string response)
        {
            if (response.Contains("Bad rconpassword."))
            {
                logger.WriteLine("Bad rcon password, please set the right password", LogImportance.Error);
                return false;
            }
            else if (response.Contains("No rconpassword"))
            {
                logger.WriteLine("The server has no password, please set the password on the server via /rconpassword <pass>", LogImportance.Error);
                return false;
            }
            else
            {
                logger.WriteLine("Successful connection", LogImportance.Success);
                return true;
            }
        }

        private string Send(Socket socket, string command)
        {
            //generates payload
            byte[] payload = GenerateByteArray(command);

            
            //send rcon command and get response
            IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
            socket.Send(payload, SocketFlags.None);

            //big enough to receive response
            byte[] bufferRec = new byte[65000 / 1000];
            
            socket.Receive(bufferRec);

            //removes 0 bytes at the end
            byte[] filtered = bufferRec.Reverse().SkipWhile(x => x > 32 && x < 126).Reverse().ToArray();
            string response = Encoding.ASCII.GetString(filtered);

            return response;
        }

        private byte[] GenerateByteArray(string toAppend)
        {
            byte[] bufferTemp = Encoding.ASCII.GetBytes(toAppend);
            byte[] bufferSend = new byte[bufferTemp.Length + 5];

            //intial 5 characters as per standard
            bufferSend[0] = byte.Parse("255");
            bufferSend[1] = byte.Parse("255");
            bufferSend[2] = byte.Parse("255");
            bufferSend[3] = byte.Parse("255");
            bufferSend[4] = byte.Parse("255");
            int j = 4;

            for (int i = 0; i < bufferTemp.Length; i++)
            {
                bufferSend[j++] = bufferTemp[i];
            }

            return bufferSend;
        }

        private void PrintResponse(string response)
        {
            int spaceAmount = 0;

            foreach (var c in response)
            {
                if (spaceAmount <= 5 && c != (char)0)
                {
                    Console.Write(c);
                    spaceAmount = 0;
                }
                else if (spaceAmount <= 5 && c == (char)0)
                {
                    Console.Write(c);
                    spaceAmount++;
                }
            }
            Console.Write("\n");
        }

        public void ClearQueue()
        {
            rconQueue.Clear();
        }
    }
}
