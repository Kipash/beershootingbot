﻿using System;
using System.Threading.Tasks;

namespace BeerShootingBot.Common
{
    public class GeolocationService: JsonServiceBase<GeolocationResults>
    {
        protected override TimeSpan MinimunFrecuency => new TimeSpan(0, 0, 1);

        private readonly string key;

        public GeolocationService(IConfigurationReader configReader, ILog logger) : base(logger)
        {
            key = configReader.ReadConfiguration().GeolocationApiKey;
        }

        protected override GeolocationResults ConsumeService(string address)
        {
            string url = $"http://api.ipinfodb.com/v3/ip-city/?ip={address}&key={key}&format=json";
            Task<GeolocationResults> task = GetExternalJson<GeolocationResults>(url);
            task.Wait();
            GeolocationResults returned = task.Result;
            return returned;
        }
    }

    public class GeolocationResults
    {
        public string countryCode;
        public string countryName;
        public string regionName;
        public string cityName;
        public string latitude;
        public string longitude;
        public string timeZone;
        public string zipCode;
    }
}
