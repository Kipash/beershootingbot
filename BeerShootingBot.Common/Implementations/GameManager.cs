﻿using System;
using System.Collections.Generic;

namespace BeerShootingBot.Common
{
    /*
    public class GameManager : IGameManager
    {
        public static Dictionary<int, Player> currentPlayers { get; private set; } = new Dictionary<int, Player>();
        public static string CurrentMap { get; set; }

        private IRconManager rcon;

        public GameManager(IRconManager rconManager)
        {
            rcon = rconManager;
        }

        public void AddPlayer(Player p)
        {
            currentPlayers.Add(p.GameSessionId, p);
            rcon.BigText($"^2+ :{p}");
        }

        public void RemovePlayer(int sessionID)
        {
            if (currentPlayers.ContainsKey(sessionID))
            {
                Player p = currentPlayers[sessionID];
                currentPlayers.Remove(sessionID);
                rcon.BigText($"^1- :{p}");
            }
        }

        public void OnKill(int killerID, int victimID, KilledBy KilledBy)
        {
            if (currentPlayers.ContainsKey(killerID) && killerID != victimID)
            {
                Player p = currentPlayers[killerID];
                p.KillsVia.Add(KilledBy);
                p.Kills++;
            }

            if (currentPlayers.ContainsKey(victimID))
            {
                Player p = currentPlayers[victimID];
                p.Deaths++;
            }
        }
    }*/
}
