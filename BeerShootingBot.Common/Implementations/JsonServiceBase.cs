﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace BeerShootingBot.Common
{
    public abstract class JsonServiceBase<ReturnedType> : IJsonService<ReturnedType>
    {
        protected abstract TimeSpan MinimunFrecuency { get; }

        private DateTime lastUsed = DateTime.Now;
        private Queue<string> pendingRequests = new Queue<string>();
        private Dictionary<string, ReturnedType> cachedResults = new Dictionary<string, ReturnedType>();
        private ILog logger;

        public event JsonServiceDelegate<ReturnedType> DataRetrieved;

        protected JsonServiceBase(ILog logger)
        {
            this.logger = logger;
            StartQueueLoop();
        }

        private async void StartQueueLoop()
        {
            while (true)
            {
                await Task.Delay(1000);
                if (DateTime.Now.Subtract(lastUsed) > MinimunFrecuency)
                {
                    // we waited enough, lets consume the service again
                    if (pendingRequests.Count > 0)
                    {
                        string ip = pendingRequests.Peek();
                        try
                        {
                            ReturnedType dataRetrieved = ConsumeService(ip);
                            if (pendingRequests.Count != 0) pendingRequests.Dequeue(); // Race condition check, could happen if !clearqueue was called
                            lastUsed = DateTime.Now;
                            cachedResults.Add(ip, dataRetrieved);
                            DataRetrieved?.Invoke(ip, dataRetrieved);
                        }
                        catch(AggregateException asynExc)
                        {
                            foreach (Exception exc in asynExc.InnerExceptions)
                            {
                                logger.WriteLine(exc.Message, LogImportance.Error);
                                logger.WriteLine(exc.StackTrace, LogImportance.Info);
                            }
                        }
                    }
                }
            }
        }

        protected abstract ReturnedType ConsumeService(string address);

        public void Enqueue(string address)
        {
            string[] localIPs = new string[] { "127.0.0.1", "localhost" };
            if (localIPs.Contains(address)) return;

            if (cachedResults.ContainsKey(address))
            {
                DataRetrieved?.Invoke(address, cachedResults[address]);
                return;
            }
            if (!pendingRequests.Contains(address)) pendingRequests.Enqueue(address);
        }

        protected async Task<T> GetExternalJson<T>(string url) where T : class
        {
            if (!Uri.TryCreate(url, UriKind.Absolute, out Uri getUri)) return default(T);

            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(getUri);
                response.EnsureSuccessStatusCode();
                string data = await response.Content.ReadAsStringAsync();
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(data);
            }
        }

        public void ClearQueue()
        {
            pendingRequests.Clear();
        }
    }
}
