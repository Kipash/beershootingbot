﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BeerShootingBot.Common
{
    public class LogParser : IEventNotifier
    {
        public event CommandReceivedDelegate CommandReceived;
        public event PlayerEnteredDelegate PlayerEntered;
        public event UnknownEventDelegate MapChanged;
        public event PluginChangedDelegate PluginChanged;
        public event PlayerTeamChangedDelegate PlayerTeamChanged;
        public event PlayerKilledDelegate PlayerKilled;
        public event PlayerLeftDelegate PlayerLeft;
        public event PlayerHitDelegate PlayerHit;
        public event PlayerDataNeededDelegate PlayerDataNeeded;

        //private FileSystemWatcher logWatcher;
        FileStream logFile;

        ILog logger;
        IConfigurationReader configReader;

        long lastPosition;

        Queue<string> linesToParse = new Queue<string>();

        // Log line format is either
        // mm:ss ----------------------------------
        // or
        // mm:ss Event: Data
        // The minutes and the seconds are relative to game start
        private static Regex lineRegex = new Regex(@"\s*\d*:\d*\s*(?<event>[a-z]*):s*(?<content>[^\n]*)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static Dictionary<LogEvent, Regex> commandRegexes = new Dictionary<LogEvent, Regex>()
        {
            { LogEvent.Say, new Regex(@"\s?(?<slot>\d+)\s(?<who>[^:]+):\s(?<said>[^\n]*)", RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { LogEvent.SayTeam, new Regex(@"\s?(?<slot>\d+)\s(?<who>[^:]+):\s(?<said>[^\n]*)", RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { LogEvent.ClientConnect, new Regex(@"\s?(?<slot>\d+)", RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { LogEvent.ClientDisconnect, new Regex(@"\s?(?<slot>\d+)", RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            // We handle ClientUserInfo and ClientUserInfoChanged in Player.GetInfoFromLogLine so no need for a complex regex in those cases
            { LogEvent.ClientUserInfo, new Regex(@"\s?(?<slot>\d+)\s*(?<keyPairs>[^\n]*)", RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { LogEvent.ClientUserInfoChanged, new Regex(@"\s?(?<slot>\d+)\s*(?<keyPairs>[^\n]*)", RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { LogEvent.Kill, new Regex(@"\s?(?<killer>\d+)\s+(?<killed>\d+)\s+(?<via>\d+):", RegexOptions.Compiled | RegexOptions.IgnoreCase) },
            { LogEvent.Hit, new Regex(@"\s?(?<victim>\d+)\s+(?<shooter>\d+)\s+(?<locationid>\d+)\s+(?<weaponid>\d+):", RegexOptions.Compiled | RegexOptions.IgnoreCase) }
            
            //TODO events: AccountValidated, InitAuth, InitGame, InitGame, InitRound, ClientAlive, PlayerScore, TeamScore, Radio, Assist, Exit, ShutDownGame, Warmup
        };

        private Dictionary<string, LogEvent> eventMappings = new Dictionary<string, LogEvent>()
        {
            { "AccountValidated", LogEvent.AccountValidated },
            { "Assist", LogEvent.Assist },
            { "ClientAlive", LogEvent.ClientAlive },
            { "ClientBegin", LogEvent.ClientBegin },
            { "ClientConnect", LogEvent.ClientConnect },
            { "ClientDead", LogEvent.ClientDead },
            { "ClientDisconnect", LogEvent.ClientDisconnect },
            { "ClientSpawn", LogEvent.ClientSpawn },
            { "ClientUserinfo", LogEvent.ClientUserInfo },
            { "ClientUserinfoChanged", LogEvent.ClientUserInfoChanged },
            { "Exit", LogEvent.Exit },
            { "Hit", LogEvent.Hit },
            { "InitAuth", LogEvent.InitAuth },
            { "InitGame", LogEvent.InitGame },
            { "InitRound", LogEvent.InitRound },
            { "Item", LogEvent.Item },
            { "Kill", LogEvent.Kill },
            { "Radio", LogEvent.Radio },
            { "say", LogEvent.Say },
            { "sayteam", LogEvent.SayTeam },
            { "score", LogEvent.PlayerScore },
            { "red", LogEvent.TeamScore },
            { "ShutdownGame", LogEvent.ShutdownGame },
            { "Warmup", LogEvent.Warmup }
        };

        public LogParser(IConfigurationReader configReader, ILog logger)
        {
            this.logger = logger;
            this.configReader = configReader;

            AppConfiguration config = configReader.ReadConfiguration();
            FileInfo logInfo = new FileInfo(config.LogFile);

            if (!logInfo.Exists)
            {
                logger.WriteLine("Server log file not found", LogImportance.Error);
                return;
            }

            //logWatcher = new FileSystemWatcher(logInfo.DirectoryName, logInfo.Name);
            //logWatcher.NotifyFilter = NotifyFilters.Attributes
            //                        | NotifyFilters.CreationTime
            //                        | NotifyFilters.DirectoryName
            //                        | NotifyFilters.FileName
            //                        | NotifyFilters.LastAccess
            //                        | NotifyFilters.LastWrite
            //                        | NotifyFilters.Security
            //                        | NotifyFilters.Size;
            //
            //logWatcher.Changed += logWatcher_Changed;
            //logWatcher.Renamed += logWatcher_Changed;
            //logWatcher.Created += logWatcher_Changed;
            //logWatcher.Deleted += logWatcher_Changed;
            //logWatcher.EnableRaisingEvents = true;
            //logWatcher.IncludeSubdirectories = true;
            //logWatcher.Filter = "*.*";

            logFile = new FileStream(config.LogFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

            LogWatchLoop();
        }

        bool initialized;
        async void LogWatchLoop()
        {
            while(true)
            {
                if(initialized)
                    CheckLogContents();
                await Task.Delay(100);
            }
        }

        public void ReadInitialStatus()
        {
            // Go back to the end of the file, then go backwards until latest game is found to load actual game info or start of log is found
            logFile.Position = logFile.Length;
            lastPosition = logFile.Length;

            long currentPosition = lastPosition;
            string currentLine = string.Empty;
            Stack<byte> currentLineBytes = new Stack<byte>();
            Stack<string> gameLines = new Stack<string>();

            byte[] bytesRead = new byte[1];

            while (currentPosition > 0 && !currentLine.StartsWith("\n  0:00 InitGame:"))
            {
                currentPosition--;
                logFile.Position = currentPosition;
                logFile.Read(bytesRead, 0, 1);
                currentLineBytes.Push(bytesRead[0]);
                // 10 is \n in UTF8
                if (bytesRead[0] == (byte)10)
                {
                    currentLine = Encoding.UTF8.GetString(currentLineBytes.ToArray());
                    currentLineBytes.Clear();
                    gameLines.Push(currentLine.Substring(1));
                }
            }
            //logWatcher.EnableRaisingEvents = true;
            if (currentPosition != 0)
            {
                // Game info found
                GetGameStatus(gameLines);
            }
            logFile.Position = lastPosition;

            initialized = true;
        }

        private void GetGameStatus(Stack<string> logLines)
        {
            // The first line contains server amount of slots, game mode and other important info
            string initLine = logLines.Pop();
            this.configReader.Game = new GameStatus(initLine);
            foreach (string logLine in logLines)
            {
                LogLine eventInfo = ParseLogLine(logLine);
                if (eventInfo != null)
                {
                    switch (eventInfo.Event)
                    {
                        case LogEvent.ClientConnect:
                        case LogEvent.ClientDisconnect:
                        case LogEvent.ClientUserInfo:
                        case LogEvent.ClientUserInfoChanged:
                            RaiseEvents(eventInfo);
                            break;
                    }
                }
            }
        }

        private LogLine ParseLogLine(string line)
        {
            if (string.IsNullOrWhiteSpace(line) || line.Contains("--------------")) return null;
            Match match = lineRegex.Match(line);
            if (match.Success)
            {
                string eventName = match.Groups["event"].Value;
                string eventContent = match.Groups["content"].Value;
                if (eventMappings.ContainsKey(eventName))
                {
                    return new LogLine() { Event = eventMappings[eventName], Content = eventContent };
                }
                else
                {
                    logger.WriteLine($"Unknown event name in log file: {eventName}");
                    return null;
                }
            }
            else
            {
                logger.WriteLine("Unknown line in log file");
                logger.WriteLine(line);
                return null;
            }
        }

        //private void logWatcher_Changed(object sender, FileSystemEventArgs e)
        //{
        void CheckLogContents()
        {
            if (lastPosition < logFile.Length)
            {
                // Do a forward reading of the new content
                int amountToRead = int.Parse((logFile.Length - lastPosition).ToString());
                byte[] bytesRead = new byte[amountToRead];
                logFile.Read(bytesRead, 0, amountToRead);
                string[] linesRead = Encoding.UTF8.GetString(bytesRead).Split('\n');
                foreach(string line in linesRead)
                {
                    if (!string.IsNullOrWhiteSpace(line.TrimEnd(' ','\0')))
                    {
                        LogLine eventInfo = ParseLogLine(line.TrimEnd(' ', '\0'));
                        if(eventInfo != null)
                            RaiseEvents(eventInfo);
                    }
                }
            }
        }

        List<int> slotsConnected = new List<int>();
        Dictionary<HitTracking, HitLocations> lastHits = new Dictionary<HitTracking, HitLocations>();

        private void RaiseEvents(LogLine eventInfo)
        {
            logger.WriteLine($"{eventInfo.Event} : {eventInfo.Content}");

            // Rest of regex are yet to be written. Cleptus is a lazy ass

            // TODO remove when all LogEvent Regex are written
            if (
               eventInfo.Event != LogEvent.Say && eventInfo.Event != LogEvent.SayTeam && eventInfo.Event != LogEvent.ClientConnect && eventInfo.Event != LogEvent.ClientDisconnect
               && eventInfo.Event != LogEvent.ClientUserInfo && eventInfo.Event != LogEvent.ClientUserInfoChanged
               && eventInfo.Event != LogEvent.Kill && eventInfo.Event != LogEvent.Hit
               ) return;

            Match matched = LogParser.commandRegexes[eventInfo.Event].Match(eventInfo.Content);
            if (!matched.Success) return;
            switch (eventInfo.Event)
            {
                case LogEvent.Say:
                case LogEvent.SayTeam:
                    {
                        string said = matched.Groups["said"].Value;
                        if (!(said.StartsWith('!') || !said.StartsWith('&'))) return;
                        int slot = int.Parse(matched.Groups["slot"].Value);
                        Player who = configReader.Game.GetPlayer(slot);
                        CommandReceived?.Invoke(who, said.TrimEnd('\0'));
                    }
                    break;
                case LogEvent.ClientConnect:
                    {
                        // This log event is not useful, but we can use it to keep track of entering users and delay PlayerEntered entered when ClientUserInfo is received
                        int slot = int.Parse(matched.Groups["slot"].Value);
                        if (!slotsConnected.Contains(slot)) slotsConnected.Add(slot);
                        configReader.Game.Players[slot] = new Player()
                        {
                            ServerSlot = slot
                        };
                    }
                    break;
                case LogEvent.ClientDisconnect:
                    {
                        int slot = int.Parse(matched.Groups["slot"].Value);

                        // Remove player from last hit tracking
                        List<HitTracking> trackingsToRemove = new List<HitTracking>();
                        foreach (HitTracking tracking in lastHits.Keys)
                        {
                            if (tracking.VictimSlot == slot || tracking.ShooterSlot == slot) trackingsToRemove.Add(tracking);
                        }
                        foreach (HitTracking tracking in trackingsToRemove)
                        {
                            lastHits.Remove(tracking);
                        }

                        // Player cleanup
                        if (configReader.Game.Players[slot] != null)
                        {
                            Player playerWhoLeft = configReader.Game.Players[slot];
                            configReader.Game.Players[slot] = null;
                            PlayerLeft?.Invoke(playerWhoLeft);
                        }
                        
                    }
                    break;
                case LogEvent.ClientUserInfo:
                    {
                        Player userInfo = Player.GetInfoFromLogLine(eventInfo);
                        if (slotsConnected.Contains(userInfo.ServerSlot.Value))
                        {
                            slotsConnected.Remove(userInfo.ServerSlot.Value);
                            Player dataRetrieved = PlayerDataNeeded?.Invoke(userInfo);
                            if (dataRetrieved != null)
                            {
                                logger.WriteLine($"{dataRetrieved.Name}: {dataRetrieved.AdminLevel}", LogImportance.Success);
                            }
                            configReader.Game.Players[userInfo.ServerSlot.Value] = dataRetrieved == null? userInfo : dataRetrieved;
                            PlayerEntered?.Invoke(dataRetrieved == null ? userInfo : dataRetrieved);
                        }
                    }
                    break;
                case LogEvent.ClientUserInfoChanged:
                    {
                        Player userInfo = Player.GetInfoFromLogLine(eventInfo);
                        GameTeam previousTeam = configReader.Game.Players[userInfo.ServerSlot.Value].Team;
                        configReader.Game.Players[userInfo.ServerSlot.Value].Team = userInfo.Team;
                        configReader.Game.Players[userInfo.ServerSlot.Value].Name = userInfo.Name;
                        if (previousTeam != userInfo.Team)
                        {
                            PlayerTeamChanged?.Invoke(configReader.Game.Players[userInfo.ServerSlot.Value], previousTeam);
                        }
                    }
                    break;
                case LogEvent.Hit:
                    {
                        Player victim = configReader.Game.Players[int.Parse(matched.Groups["victim"].Value)];
                        Player shooter = configReader.Game.Players[int.Parse(matched.Groups["shooter"].Value)];
                        HitLocations locationId = Enum.Parse<HitLocations>(matched.Groups["locationid"].Value);
                        HitItem weaponId = Enum.Parse<HitItem>(matched.Groups["locationid"].Value);

                        // Store last victim-shooter hit location to provide LogEvent. Kill the location of the last hit of the alleged killer
                        HitTracking tracking = new HitTracking()
                        {
                            ShooterSlot = shooter.ServerSlot.Value,
                            VictimSlot = victim.ServerSlot.Value
                        };
                        if (!lastHits.ContainsKey(tracking)) lastHits.Add(tracking, locationId);
                        else lastHits[tracking] = locationId;

                        PlayerHit?.Invoke(shooter, victim, weaponId, locationId);
                    }
                    break;
                case LogEvent.Kill:
                    {
                        Player killer = configReader.Game.Players[int.Parse(matched.Groups["killer"].Value)];
                        Player killed = configReader.Game.Players[int.Parse(matched.Groups["killed"].Value)];
                        KilledBy via = Enum.Parse<KilledBy>(matched.Groups["via"].Value);
                        HitLocations location = HitLocations.None;
                        if (via.HasHitAssociated())
                        {
                            HitTracking killerKilled = new HitTracking()
                            {
                                ShooterSlot = killer.ServerSlot.Value,
                                VictimSlot = killed.ServerSlot.Value
                            };
                            if (lastHits.ContainsKey(killerKilled))
                            {
                                location = lastHits[killerKilled];
                                lastHits.Remove(killerKilled);
                            }
                        }

                        PlayerKilled?.Invoke(killer, killed, via, location);
                    }
                    break;
            }
        }

        public void ChangePlugin(string pluginName, bool enabled)
        {
            PluginChanged?.Invoke(pluginName, enabled);
        }

        internal class LogLine
        {
            internal LogEvent Event { get; set; }
            internal string Content { get; set; }
        }

        internal struct HitTracking
        {
            internal int ShooterSlot { get; set; }
            internal int VictimSlot { get; set; }
        }
    }

   
}
