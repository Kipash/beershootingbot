﻿namespace BeerShootingBot.Common
{
    public enum HitItem
    {
        None = 0,
        Knife = 1,
        Beretta = 2,
        Deagle = 3,
        Spas = 4,
        Mp5 = 5,
        Ump45 = 6,
        Lr300 = 8,
        G36 = 9,
        Psg1 = 10,
        Sr8 = 14,
        Ak103 = 15,
        Negev = 17,
        M4 = 19,
        Glock = 20,
        Colt1911 = 21,
        Mac11 = 22,
        Frf1 = 23,
        Benelli = 24,
        P90 = 25,
        Magnum = 26
    }
}
