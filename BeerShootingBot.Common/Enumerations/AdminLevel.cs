﻿using System.ComponentModel;

namespace BeerShootingBot.Common
{
    public enum AdminLevel
    {
        [Description("Guest")]
        Guest = 0,
        [Description("Registered user")]
        RegisteredUser = 1,
        [Description("Regular")] // Long time players, friendly fire control does not ban them, only kicks them
        Regular = 2,
        [Description("Moderator")]
        Moderator = 20,
        [Description("Friend admin")]
        FriendAdmin = 40,
        [Description("Clan member")]
        Member = 60,
        [Description("Bot support")]
        BotAdmin = 80,
        [Description("Clan head")] // Can manage who is in what groups
        God = 90
    }   
}
