﻿using System;

namespace BeerShootingBot.Common
{
    public enum HitLocations
    {
        /// <summary>
        /// Hit location not specified (for example nades)
        /// </summary>
        [HitLocation("not specified")] None = 0,
        [HitLocation("head", isHeadshoot: true)] Head = 1,
        [HitLocation("helmet", isHeadshoot: true)] Helmet = 2,
        [HitLocation("torso")] Torso = 3,
        [HitLocation("vest")] Vest = 4,
        [HitLocation("left arm")] LeftArm = 5,
        [HitLocation("right arm")] RightArm = 6,
        [HitLocation("groin")] Groin = 7,
        [HitLocation("butt")] Butt = 8,
        [HitLocation("left upper leg")] LeftUpperLeg = 9,
        [HitLocation("right upper leg")] RightUpperLeg = 10,
        [HitLocation("left lower leg")] LeftLowerLeg = 11,
        [HitLocation("right lower leg")] RightLowerLeg = 12,
        [HitLocation("left foot")] LeftFoot = 13,
        [HitLocation("right foot")] RightFood = 14
    }

    public class HitLocationAttribute : Attribute
    {
        public string Name { get; private set; }
        /// <summary>
        /// Used to let stats plugin know easily what is a HS.
        /// </summary>
        public bool IsHeadshoot { get; private set; }

        public HitLocationAttribute(string name, bool isHeadshoot = false)
        {
            Name = name;
            IsHeadshoot = isHeadshoot;
        }
    }
}
