﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerShootingBot.Common
{
    public enum LogImportance
    {
        Normal = 1,

        Info = 2,
        Success = 3,
        Error = 4,
    }
}
