﻿namespace BeerShootingBot.Common
{
    internal enum LogEvent
    {
        AccountValidated, // careful, different format
        InitAuth, 
        InitGame,
        InitRound, // Warmup ended, game starts
        ClientConnect, //user disconnected
        ClientUserInfo, //user info received
        ClientUserInfoChanged, //user info changed
        ClientDisconnect, //user disconnected
        ClientBegin,// This provides no useful info
        ClientSpawn, // This provides no useful info, happens just before alive
        ClientAlive, // Slot coordinates
        Hit, // Slot_victim slot_shooter location weapon?
        Kill, // Slot_victim slot_killer weapon_or_change_team
        ClientDead, // Slot coordinates
        Item, // slot weapon. Weapon picked???
        Say, //public chat. Slot nick: message
        SayTeam, //team chat. Slot nick: message
        PlayerScore,
        TeamScore,
        Radio, // slot - type - subtype - "location" - "radio message"
        Assist, // slot_helper slot_assisted slot_killed
        Exit, //match ends
        ShutdownGame,//match ends
        Warmup
    }
}
