﻿using System;

namespace BeerShootingBot.Common
{
    public enum KilledBy
    {
        [KilledBy("water", HitItem.None)] UT_MOD_WATER = 1,
        [KilledBy("lava", HitItem.None)] UT_MOD_LAVA = 3,
        [KilledBy("telefrag", HitItem.None)] UT_MOD_TELEFRAG = 5,
        [KilledBy("falling", HitItem.None)] UT_MOD_FALLING = 6,
        [KilledBy("suicide", HitItem.None)] UT_MOD_SUICIDE = 7,
        [KilledBy("trigger hurt", HitItem.None)] UT_MOD_TRIGGER_HURT = 9, // Strange name, wtf is that?
        [KilledBy("change team", HitItem.None)] UT_MOD_CHANGE_TEAM = 10,
        [KilledBy("knife", HitItem.Knife)] UT_MOD_KNIFE = 12,
        [KilledBy("knife thrown", HitItem.Knife)] UT_MOD_KNIFE_THROWN = 13,
        [KilledBy("beretta", HitItem.Beretta)] UT_MOD_BERETTA = 14,
        [KilledBy("de", HitItem.Deagle)] UT_MOD_DEAGLE = 15,
        [KilledBy("spas", HitItem.Spas)] UT_MOD_SPAS = 16,
        [KilledBy("ump", HitItem.Ump45)] UT_MOD_UMP45 = 17,
        [KilledBy("mp5", HitItem.Mp5)] UT_MOD_MP5K = 18,
        [KilledBy("lr", HitItem.Lr300)] UT_MOD_LR300 = 19,
        [KilledBy("g36", HitItem.G36)] UT_MOD_G36 = 20,
        [KilledBy("psg", HitItem.Psg1)] UT_MOD_PSG1 = 21,
        [KilledBy("hk69", HitItem.None)] UT_MOD_HK69 = 22,
        [KilledBy("bleed", HitItem.None)] UT_MOD_BLED = 23,
        [KilledBy("kicked", HitItem.None)] UT_MOD_KICKED = 24,
        [KilledBy("he", HitItem.None)] UT_MOD_HEGRENADE = 25,
        [KilledBy("sr8", HitItem.Sr8)] UT_MOD_SR8 = 28,
        [KilledBy("ak", HitItem.Ak103)] UT_MOD_AK103 = 30,
        [KilledBy("sploded", HitItem.None)] UT_MOD_SPLODED = 31,
        [KilledBy("slapped", HitItem.None)] UT_MOD_SPLAPPED = 32,
        [KilledBy("smited", HitItem.None)] UT_MOD_SMITED = 33,
        [KilledBy("bombed", HitItem.None)] UT_MOD_BOMBED = 34,
        [KilledBy("nuked", HitItem.None)] UT_MOD_NUKED = 35,
        [KilledBy("negev", HitItem.Negev)] UT_MOD_NEGEV = 36,
        [KilledBy("direct hk69", HitItem.None)] UT_MOD_HK69_HIT = 37,
        [KilledBy("m4", HitItem.M4)] UT_MOD_M4 = 38,
        [KilledBy("glock", HitItem.Glock)] UT_MOD_GLOCK = 39,
        [KilledBy("colt", HitItem.Colt1911)] UT_MOD_COLT1911 = 40,
        [KilledBy("mac", HitItem.Mac11)] UT_MOD_MAC11 = 41,
        [KilledBy("frf1", HitItem.Frf1)] UT_MOD_FRF1 = 42,
        [KilledBy("benelli", HitItem.Benelli)] UT_MOD_BENELLI = 43,
        [KilledBy("p90", HitItem.P90)] UT_MOD_P90 = 44,
        [KilledBy("magnum", HitItem.Magnum)] UT_MOD_MAGNUM = 45,
        [KilledBy("50 cal", HitItem.None)] UT_MOD_TOD50 = 46, //TODO Get logs of tod50 hits
        [KilledBy("flag", HitItem.None)] UT_MOD_FLAG = 47,
        [KilledBy("goomba", HitItem.None)] UT_MOD_GOOMBA = 48
    }

    public class KilledByAttribute : Attribute
    {
        public string Name { get; private set; }

        public HitItem RegisteredHit { get; private set; }

        public KilledByAttribute(string name, HitItem registeredHit)
        {
            Name = name;
            RegisteredHit = registeredHit;
        }
    }
}
