﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerShootingBot.Common
{
    public class GameStatus
    {
        public Player[] Players = null;
        public string MapName { get; private set; }
        public GameStatus(string initLine)
        {
            MapName = string.Empty;
            string[] parts = initLine.Trim().Substring(16).Split('\\');
            string name = string.Empty;
            Dictionary<string, string> keyValues = new Dictionary<string, string>();
            for(int i = 0; i< parts.Length; i++)
            {
                if (i == 0 || i % 2 == 0) name = parts[i];
                else keyValues.Add(name, parts[i]);
            }
            foreach(string key in keyValues.Keys)
            {
                switch(key)
                {
                    case "sv_maxclients":
                        int maxSlots = int.Parse(keyValues[key]);
                        Players = new Player[maxSlots];
                        break;
                    case "mapname":
                        MapName = keyValues[key];
                        break;
                }
            }
            // TODO There may be something useful in keyValues, atm we only get number of slots
        }

        public Player GetPlayer(int slotNumber)
        {
            if (slotNumber < 0 || slotNumber > Players.Length) return null;
            return Players[slotNumber];
        }
        public Player GetPlayer(string nick)
        {
            return Players.Where(p => p != null && p.Name == nick).FirstOrDefault();
        }

        public Player SearchPlayerByGuid(string guid)
        {
            return Players.Where(p => p != null && p.Guid == guid).SingleOrDefault();
        }

        /// <summary>
        /// Search a server connected player
        /// </summary>
        /// <param name="searchTerm">@playerId|slotNumber|nick|alias</param>
        /// <returns></returns>
        public Player SearchPlayer(string searchTerm)
        {
            if (searchTerm.StartsWith('@'))
            {
                searchTerm = searchTerm.Substring(1);
                if (long.TryParse(searchTerm, out long userId))
                {
                    return Players.Where(p => p != null && p.UserId.HasValue && p.UserId.Value == userId).FirstOrDefault();
                }
                return null;
            }
            if (int.TryParse(searchTerm, out int slotNumber))
            {
                return Players[slotNumber];
            }
            Player[] foundByNickName = Players.Where
            (p =>
                p != null &&
                (
                    p.Name.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase)
                    || (!string.IsNullOrEmpty(p.Auth) && p.Auth.Contains(searchTerm, StringComparison.CurrentCultureIgnoreCase))
                )
            ).ToArray();
            if (foundByNickName.Length == 1) return foundByNickName[0];
            else return null;
        }
    }
}
