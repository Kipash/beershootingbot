﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerShootingBot.Common
{
    public enum Cvars
    {
        none = 0,
        g_gravity,
    }
    /// <summary>
    /// Get or set console variables - Cvars
    /// </summary>
    public class CvarManager
    {
        //TODO: Dependency injection magic - get IRconManager instance
        IRconManager rcon;

        //Suggestion, bind Type and cvar name, so GetCvar would have return-type object.
        //That return value could be casted to int or string as needed.

        //Response good name, good value:           Server: g_gravity changed to -10
        //Response bad name:                        Server: g_gravit 
        public void SetCvar(Cvars cvarType, string arguments)
        {
            //TODO: Resolve asynchronus getting and setting, because rcon execution queue is asynchronus
            // issue #1 - every implementation of every plugin have to be async
            // issue #2 - 

            //rcon.QueueCommand($"{cvarType} {arguments}")
        }

        public string GetCvar(Cvars cvarType)
        {
            //not possible
            //var response = rcon.QueueCommand($"{cvarType}")
            return "not implemented";
        }
    }
}
