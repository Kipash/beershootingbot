﻿namespace BeerShootingBot.Common
{
    //Needs to be public so Newtonsoft works
    public class AppConfiguration
    {
        public AppConfigurationDatabase Database { get; set; }
        public bool? BalancingEnabled { get; set; }
        public bool? PlayerWatcherEnabled { get; set; }
        public string LogFile { get; set; }
        public AppConfigurationRcon Rcon { get; set; }
        public string GeolocationApiKey { get; set; }
        public Commandlevels CommandLevels { get; set; }
        public Component[] Components { get; set; }

        public class Component
        {
            public string Interface { get; set; }
            public string Type { get; set; }
            public string Implementation { get; set; }
        }

        public class Commandlevels
        {
            public string[] Guest { get; set; }
            public string[] RegisteredUser { get; set; }
            public string[] Moderator { get; set; }
            public string[] FriendAdmin { get; set; }
            public string[] Member { get; set; }
            public string[] BotAdmin { get; set; }
            public string[] God { get; set; }
        }

        public class AppConfigurationRcon
        {
            public int Delay { get; set; }
            public string Password { get; set; }
            public int RconPort { get; set; }
            public string IP { get; set; }
        }

        public class AppConfigurationDatabase
        {
            public string ProviderName { get; set; }
            public string ConnectionString { get; set; }
        }

        /// <summary>
        /// Override data with more dominate instance.
        /// </summary>
        /// <param name="instance"></param>
        public void OverrideData(AppConfiguration instance)
        {
            if (instance.Database != null)
                Database = instance.Database;
            if (instance.BalancingEnabled.HasValue)
                BalancingEnabled = instance.BalancingEnabled ;
            if (instance.PlayerWatcherEnabled.HasValue)
                PlayerWatcherEnabled = instance.PlayerWatcherEnabled.Value;
            if (!string.IsNullOrWhiteSpace(instance.LogFile))
                LogFile = instance.LogFile;
            if (instance.Rcon != null)
                Rcon = instance.Rcon;
            if (string.IsNullOrWhiteSpace(instance.GeolocationApiKey))
                GeolocationApiKey = instance.GeolocationApiKey;
            if (instance.CommandLevels != null)
                CommandLevels = instance.CommandLevels;
            //if (instance.Components != null)
            //    Components = instance.Components;
        }
    }
}
