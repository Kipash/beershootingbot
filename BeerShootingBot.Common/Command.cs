﻿using System;

namespace BeerShootingBot.Common
{
    public sealed class Command
    {
        public string Description { get; set; }
        public string Alias { get; set; }
        public string Syntax { get; set; }
        public AdminLevel MinimunLevel { get; set; } = AdminLevel.God;
        
        public Func<Player, string[], string> Execute;

        public string ToString(string commandName)
        {
            string returned = commandName;
            if (Alias != null) returned += $"/{Alias}";
            returned += $":{Description}";
            return returned;
        }
    }
}
