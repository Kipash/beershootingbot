﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerShootingBot.Common
{
    public class Player
    {
        public bool IsBot;

        public int? ServerSlot;
        public AdminLevel AdminLevel = AdminLevel.Guest;
        public string IP = "";
        public string Name = "";
        public string Auth = null;
        public string Guid = "";
        public DateTime LastPlayed = DateTime.Now;
        public long? RegisteredId; // Database xlrstats.id
        public long? UserId; // Database player.id
        public bool? UsingProxyOrVPN;
        public string FollowedReason; // Null if it is not followed
        public string Country;
        public string City;

        public GameTeam Team = GameTeam.none;

        public int Kills = 0;
        public int Deaths = 0;

        public List<KilledBy> KillsVia = new List<KilledBy>();

        public override string ToString()
        {
            return $"{Name}({ServerSlot})-Auth:{Auth}-Team:{Team}-Bot:{IsBot}-PlayerId:@{UserId}-Registered:{RegisteredId.HasValue}-Group:{AdminLevel.GetDescription()}";
        }

        internal static Player GetInfoFromLogLine(LogParser.LogLine lineInfo)
        {
            Player returned = new Player();
            string content = lineInfo.Content.Trim();
            int slot = int.Parse(content.Substring(0, content.IndexOf(' ')));
            returned.ServerSlot = slot;
            string[] parts = (lineInfo.Event == LogEvent.ClientUserInfo) ? content.Substring(content.IndexOf(' ') + 2).Split('\\') : content.Substring(content.IndexOf(' ') + 1).Split('\\');
            string name = string.Empty;
            Dictionary<string, string> keyValues = new Dictionary<string, string>();
            for (int i = 0; i < parts.Length; i++)
            {
                if (i == 0 || i % 2 == 0) name = parts[i];
                else keyValues.Add(name, parts[i]);
            }
            if (lineInfo.Event == LogEvent.ClientUserInfo)
            {
                if (keyValues.ContainsKey("skill"))
                {
                    returned.IsBot = true;
                    returned.IP = "localhost";
                }
                else
                {
                    if (keyValues["ip"] == "localhost") returned.IP = "127.0.0.1";
                    else returned.IP = keyValues["ip"].Substring(0, keyValues["ip"].IndexOf(':'));
                    returned.Guid = keyValues["cl_guid"];
                    if (keyValues.ContainsKey("authl")) returned.Auth = keyValues["authl"];
                }
                returned.Name = keyValues["name"];
                
            }
            else
            {
                if (keyValues.ContainsKey("n") && returned.Name != keyValues["n"]) returned.Name = keyValues["n"];
                returned.Team = Enum.Parse<GameTeam>(keyValues["t"]);
            }
            return returned;
        }
    }
}
