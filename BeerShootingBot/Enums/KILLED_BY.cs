﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerShootingBot {
    public enum KILLED_BY 
    {
        none = 0,
        UT_MOD_KNIFE,
        UT_MOD_KNIFE_THROWN,

        UT_MOD_DEAGLE,
        UT_MOD_BERETTA,
        UT_MOD_COLT1911,

        UT_MOD_UMP45,
        UT_MOD_MP5K,
        UT_MOD_SPAS,

        UT_MOD_M4,
        UT_MOD_AK103,
        UT_MOD_G36,
        UT_MOD_LR300,
        UT_MOD_NEGEV,

        UT_MOD_PSG1,
        UT_MOD_SR8,

        UT_MOD_HK69,
        UT_MOD_HK69_HIT,

        UT_MOD_HEGRENADE,


        UT_MOD_BLED,
        MOD_CHANGE_TEAM,
        MOD_SUICIDE
    }
}
