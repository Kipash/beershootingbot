﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerShootingBot
{
    public enum GameTeam
    {
        none = 0,
        Red = 1,
        Blue = 2,
        Spectator = 3,
    }
}
