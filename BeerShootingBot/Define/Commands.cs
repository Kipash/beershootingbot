﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerShootingBot
{
    public static class Commands
    {
        public static void Initialize()
        {
            Parser.Commands.Add("moon",
            new Func<int, string, bool>[]
            {
                (level, parametrs) =>
                {
                    var sections = parametrs.Split(' ');
                    if(sections.Length == 1)
                    {
                        var p = sections[0].ToLower();

                        if (p == "on")
                            RconManager.QueueCommand("g_gravity 100");
                        else if (p == "off")
                            RconManager.QueueCommand("g_gravity 800");
                    }

                    return true;
                }
            });

            Parser.Commands.Add("myrcon",
            new Func<int, string, bool>[]
            {
                (level, parametrs) =>
                {
                    RconManager.QueueCommand(parametrs);
                    return true;
                }
            });

            Parser.Commands.Add("vote_enable",
            new Func<int, string, bool>[]
            {
                (level, parametrs) =>
                {
                    if(parametrs == "on")
                    {
                        RconManager.QueueCommand("g_allowvote 536871039");
                        RconManager.BigText("^2Votes enabled");
                    }

                    else if(parametrs == "off")
                    {
                        RconManager.QueueCommand("g_allowvote 0");
                        RconManager.BigText("^1Votes disabled");
                    }

                    return true;
                }
            });

            Parser.Commands.Add("cvote",
            new Func<int, string, bool>[]
            {
                (level, parametrs) =>
                {
                    RconManager.QueueCommand($"callvote {parametrs}");
                    return true;
                }
            });

            Parser.Commands.Add("score",
            new Func<int, string, bool>[]
            {
                (level, parametrs) =>
                {
                    foreach(var x in GameManager.currentPlayers)
                    {
                        RconManager.BigText($"^5{x.Value.Name}({x.Key}) - k:{x.Value.Kills} d:{x.Value.Deaths}");
                    }
                    return true;
                }
            });

            Parser.Commands.Add("welcome",
            new Func<int, string, bool>[]
            {
                (chatterID, parametrs) =>
                {
                    Player pl1 = GameManager.currentPlayers.Values.FirstOrDefault(x => x.GameSessionId == chatterID);
                    Player pl2 = GameManager.currentPlayers.Values.FirstOrDefault(x => x.Name.ToLower().Contains(parametrs.ToLower()));
                    if(pl1 != null && pl2 != null)
                    {
                        RconManager.BigText($"^1{pl1.Name} ^7welcomes ^2{pl2.Name}");
                    }
                    return true;
                }
            });

            Parser.Commands.Add("addbot",
            new Func<int, string, bool>[]
            {
                (level, parametrs) =>
                {
                    Console.WriteLine("Adding bot");
                    RconManager.QueueCommand("addbot Puma 4 Red 76 =lvl4=Puma");
                    //GameManager.AddPlayer(new Player() { Name = "=lvl4=Puma", })
                    return true;
                }
            });

            Parser.Commands.Add("about",
            new Func<int, string, bool>[]
            {
                (level, parametrs) =>
                {
                    Player pl = GameManager.currentPlayers.Values.FirstOrDefault(x => x.Name.ToLower().Contains(parametrs.ToLower()));
                    if(pl != null)
                    {
                        RconManager.BigText(pl.ToString());
                    }
                    return true;
                }
            });
        }
    }
}
