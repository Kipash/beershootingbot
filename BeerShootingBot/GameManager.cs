﻿using System.Collections.Generic;

namespace BeerShootingBot
{
    public class GameManager
    {
        public static Dictionary<int, Player> currentPlayers { get; private set; } = new Dictionary<int, Player>();
        public static string CurrentMap { get; set; }

        public static void AddPlayer(Player p)
        {
            currentPlayers.Add(p.GameSessionId, p);
            RconManager.BigText($"^2+ :{p}");
        }

        public static void RemovePlayer(int sessionID)
        {
            if (currentPlayers.ContainsKey(sessionID))
            {
                Player p = currentPlayers[sessionID];
                currentPlayers.Remove(sessionID);
                RconManager.BigText($"^1- :{p}");
            }
        }

        public static void OnKill(int killerID, int victimID, KILLED_BY killed_by)
        {
            if (currentPlayers.ContainsKey(killerID) && killerID != victimID)
            {
                Player p = currentPlayers[killerID];
                p.KillsVia.Add(killed_by);
                p.Kills++;
            }

            if (currentPlayers.ContainsKey(victimID))
            {
                Player p = currentPlayers[victimID];
                p.Deaths++;
            }
        }
    }
}
