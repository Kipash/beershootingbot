﻿using BeerShootingBot.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BeerShootingBot.Balancing
{
    public class Balancing : PluginBase
    {
        public override string Name => "Balancing";

        public override string Version => "0.1";

        public Balancing(IConfigurationReader configReader, IEventNotifier eventNotifier, IRconManager rconManager, IDatabaseProvider databaseProvider) : base(configReader, eventNotifier, rconManager, databaseProvider)
        {
            eventNotifier.PluginChanged += EventNotifier_PluginChanged;
        }

        private void EventNotifier_PluginChanged(string pluginName, bool enabled)
        {
            if (!Name.Equals(pluginName, StringComparison.CurrentCultureIgnoreCase)) return;
            currentEnabled = enabled;
        }

        protected override Dictionary<string, Command> GetCommands() => new Dictionary<string, Command>()
        {
            {
                "teams",
                new Command()
                {
                    Description = "Makes teams a similar number of players, for a skill balance check !balance",
                    Syntax = "teams",
                    Execute = Teams
                }
            }
        };

        protected override bool GetEnabled(AppConfiguration config)
        {
            return config.BalancingEnabled.Value;
        }

        private string Teams(Player sender, string[] arguments)
        {
            if (arguments.Length != 0) return $"Wrong command syntax, command syntax is: {Commands["teams"].Syntax}";
            int redPlayers = configReader.Game.Players.Count(p => p?.Team == GameTeam.Red);
            int bluePlayers = configReader.Game.Players.Count(p => p?.Team == GameTeam.Blue);
            int difference = Math.Abs(redPlayers - bluePlayers);
            if (difference < 2) return "Not needed. Teams already have similar number of players";
            GameTeam excessiveTeam = redPlayers > bluePlayers ? GameTeam.Red : GameTeam.Blue;
            List<Player> playersToMove = new List<Player>();
            // TODO: Choose a better sorting way
            List<Player> orderedPlayers = configReader.Game.Players.Where(p => p?.Team == excessiveTeam).OrderByDescending(p => p.Guid).ToList();
            while(difference >= 2)
            {
                Player playerToMove = orderedPlayers.First();
                orderedPlayers.Remove(playerToMove);
                playersToMove.Add(playerToMove);
                difference--;
            }
            foreach(Player playerToMove in playersToMove)
            {
                ForcePlayer(playerToMove, playerToMove.Team == GameTeam.Blue ? GameTeam.Red : GameTeam.Blue);
            }
            rcon.BigText("Teams balanced!");
            return string.Empty;
        }
    }
}
