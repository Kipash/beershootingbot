﻿using BeerShootingBot.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BeerShootingBot.Administration
{
    /// <summary>
    /// 
    /// </summary>
    public class Administration : PluginBase, IQueue
    {
        public override string Name => "Administration";

        public override string Version => "0.1";

        protected override Dictionary<string, Command> GetCommands() => new Dictionary<string, Command>()
        {
            // Guest level commands
            // - TODO: time
            {
                "admins",
                new Command()
                {
                    Description = "Gets online admins",
                    Syntax = "admins",
                    Execute = Admins
                }
            },
            {
                "help",
                new Command()
                {
                    Description = "Displays all available commands",
                    Syntax = "help [command]",
                    Execute = Help

                }
            },
            {
                "register",
                new Command()
                {
                    Description = "Registers yourself as a basic user",
                    Syntax = "register",
                    Execute = Register
                }
            },

            // Registered user commands
            // - TODO: currentmaps
            {
                "myinfo",
                new Command()
                {
                    Description = "Shows your user group",
                    Syntax = "myinfo",
                    Execute = MyInfo
                }
            },
            {
                "oneringtorulethemall",
                new Command()
                {
                    Description = $"Gives a registered user at '{AdminLevel.God.GetDescription()}' level. Can only be executed once",
                    Syntax = "oneringtorulethemall",
                    Execute = RingOfPower
                }
            },

            // Moderator commands
            // - TODO: addbots, admins, afk, aliases, ci, exit(?), warn

            {
                "lookup",
                new Command()
                {
                    Description = "Looks a player in the bot database",
                    Syntax = "lookup @registeredId|name",
                    Execute = Lookup
                }
            },
            {
                "list",
                new Command()
                {
                    Description = "Gets current players information",
                    Syntax = "list",
                    Execute = List
                }
            },
            {
                "pluginlist",
                new Command()
                {
                    Description = "Get info of server plugins and related commands",
                    Syntax = "pluginlist",
                    Execute = PluginList
                }
            },
            // Admin commands
            // - TODO: follow, status, unfollow, map, swap, tell

            // Bot management commands
            // - TODO: version
            {
                "pluginenable",
                new Command()
                {
                    Description = "Enables or disables a specific plugin. Note: administration plugin cannot be disabled",
                    Syntax = "pluginenable pluginName on|off",
                    Execute = PluginEnable
                }
            },
            {
                "clearqueue",
                new Command()
                {
                    Description = "Clears RCON and other (vpn/locate) queues",
                    Syntax = "clearqueue",
                    Execute = ClearQueue
                }
            },
            {
                "databaseselect",
                new Command()
                {
                    Description = "Executes a SELECT statement against the database and private messages the result",
                    Syntax = "databaseselect select statement",
                    Execute = DatabaseSelect
                }
            },

            // Clan head commands
            {
                "putgroup",
                new Command()
                {
                    Description = "Add a registered user to a group",
                    Syntax = "putgroup @registeredId group_name",
                    Execute = PutGroup
                }
            },
            {
                "ungroup",
                new Command()
                {
                    Description = $"Set a user back to '{AdminLevel.RegisteredUser.GetDescription()}' group",
                    Syntax = "ungroup @registeredId",
                    Execute = Ungroup
                }
            },
        };

        private AdminLevel? GetLevel(string search)
        {
            string[] enumValues = Enum.GetNames(typeof(AdminLevel));
            List<AdminLevel> foundLevels = new List<AdminLevel>();
            foreach (string enumValue in enumValues)
            {
                AdminLevel lvl = Enum.Parse<AdminLevel>(enumValue);
                if (lvl.GetDescription().Contains(search, StringComparison.CurrentCultureIgnoreCase)) foundLevels.Add(lvl);
            }
            if (foundLevels.Count != 1) return null;
            return foundLevels[0];
        }

        // This plugin can never be disabled
        protected override bool GetEnabled(AppConfiguration config)
        {
            return true;
        }

        public Administration(IConfigurationReader configReader, IEventNotifier eventNotifier, IRconManager rconManager, IDatabaseProvider databaseProvider) : base(configReader, eventNotifier, rconManager, databaseProvider)
        {
            bool godRegistered = false;
            using (IDbCommand command = db.GetCommand())
            {
                command.Text($"select guid from xlrstats where admin_role = {(int)AdminLevel.God}");
                object found = command.ExecuteScalar();
                command.Connection.Close();
                if (found != null && found != DBNull.Value)
                {
                    godRegistered = true;
                }
            }

            if (godRegistered) Commands.Remove("oneringtorulethemall");

            eventNotifier.PlayerKilled += EventNotifier_PlayerKilled;
            eventNotifier.PlayerDataNeeded += EventNotifier_PlayerDataNeeded;

            rconManager.PlayerRetrieved += RconManager_PlayerRetrieved;
        }

        private void RconManager_PlayerRetrieved(Player[] onlinePlayers)
        {
            // Update configReader.Game.Players array. To do the lookup we can only use the name
        }

        private Player EventNotifier_PlayerDataNeeded(Player newPlayer)
        {
            if (!newPlayer.IsBot)
            {
                IDbCommand command = db.GetCommand();
                long? userId;
                AdminLevel userLevel = AdminLevel.Guest;
                IDataReader reader = command
                    .Text("select p.id as userId, x.id as registeredId, x.admin_role from player p left join xlrstats x on p.guid = x.guid where p.guid = @guid")
                    .AddParameter("guid", newPlayer.Guid)
                    .ExecuteReader();
                if (reader.Read())
                {
                    userId = (long)reader["userId"];
                    if (reader["registeredId"] != DBNull.Value)
                    {
                        newPlayer.RegisteredId = (long)reader["registeredId"];
                        userLevel = Enum.Parse<AdminLevel>(reader["admin_role"].ToString());
                        newPlayer.AdminLevel = userLevel;
                    }
                    reader.Close();
                }
                else
                {
                    reader.Close();
                    long? maxId = command
                        .Text("select max(id) from player")
                        .ExecuteScalar() as long?;
                    if (!maxId.HasValue) maxId = 0;
                    maxId++;
                    command
                        .Text("insert into player(id, guid, name, ip_address, time_joined, aliases) values (@id, @guid, @name, @ip_address, @time_joined, @aliases)")
                        .AddParameter("id", maxId.Value)
                        .AddParameter("guid", newPlayer.Guid)
                        .AddParameter("name", newPlayer.Name)
                        .AddParameter("ip_address", newPlayer.IP)
                        .AddParameter("time_joined", DateTime.Now)
                        .AddParameter("aliases", newPlayer.Name)
                        .ExecuteNonQuery();
                    userId = maxId;
                }
                newPlayer.UserId = userId;
                command.Connection.Close();
            }
            return newPlayer;
        }

        private void EventNotifier_PlayerKilled(Player killer, Player killed, KilledBy via, HitLocations location)
        {
            // Database XLStats update
            string bigtext = $"{killer.Name} killed {killed.Name} with {via}";
            if (via.HasHitAssociated())
            {
                bigtext += $" with {via.GetName()}";
                bool inTheHead = location.IsHeadshoot();
                bigtext += inTheHead ? " and was a HS!" : " but missed the head";
            }
            rcon.BigText(bigtext);
        }

        private string ChangeUserLevel(Player player, AdminLevel newLevel)
        {
            IDbCommand command = db.GetCommand();
            command
                .Text("update xlrstats set admin_role = @admin_role where guid = @guid")
                .AddParameter("guid", player.Guid)
                .AddParameter("admin_role", (long)newLevel)
                .ExecuteNonQuery();
            player.AdminLevel = newLevel;
            Player foundPlayer = configReader.Game.Players.SingleOrDefault(p => p != null && p.RegisteredId == player.RegisteredId);
            if (foundPlayer != null && player.AdminLevel != newLevel) foundPlayer.AdminLevel = newLevel;
            return $"The new group of {player.ToString()} is {newLevel.GetDescription()}";
        }

        #region Plugin commands implementation
        private string Admins(Player sender, string[] arguments)
        {
            if (arguments.Length != 0) return $"Wrong command syntax, command syntax is: {Commands["admins"].Syntax}";
            Player[] onlineAdmins = GetOnlinePlayers(p => p.AdminLevel >= AdminLevel.FriendAdmin);
            return "Online admins: " + string.Join(' ', (from admin in onlineAdmins select $"{admin.Name}({admin.AdminLevel.GetDescription()})").ToArray());
        }
        private string Help(Player sender, string[] arguments)
        {
            Dictionary<string, Command> senderLevelCommands = new Dictionary<string, Command>();
            foreach (IPlugin plugin in configReader.Plugins)
            {
                if (plugin.IsEnabled)
                {
                    var pluginLevelCommands = (from pair in plugin.Commands where (int)pair.Value.MinimunLevel <= (int)sender.AdminLevel select pair).ToList();
                    foreach(KeyValuePair<string, Command> pluginLevelCommand in pluginLevelCommands) senderLevelCommands.Add(pluginLevelCommand.Key, pluginLevelCommand.Value);
                }
            }
            if (sender.AdminLevel > AdminLevel.Guest) senderLevelCommands.Remove("register");
            switch (arguments.Length)
            {
                case 0:
                    // Regular "!help", just return all the names of the commands
                    return string.Join(' ', senderLevelCommands.Keys);
                case 1:
                    // Show detailed info for a specific command "!help commandName"
                    var pairFound = senderLevelCommands.SingleOrDefault(p => p.Key.Equals(arguments[0], StringComparison.CurrentCultureIgnoreCase) || (!string.IsNullOrEmpty(p.Value.Alias) && p.Value.Alias.Equals(arguments[0], StringComparison.CurrentCultureIgnoreCase)));
                    if (pairFound.Equals(default(KeyValuePair<string, Command>))) return "Command not found or user has not enough rights. Type !help for the list of available commands";
                    return pairFound.Value.ToString();
                default:
                    return $"Wrong command syntax, command syntax is: {Commands["help"].Syntax}";
            }
        }
        private string Register(Player sender, string[] arguments)
        {
            if (arguments.Length != 0) return $"Wrong command syntax, command syntax is: {Commands["register"].Syntax}";
            if (sender.RegisteredId.HasValue) return "You were already registered. Type !myinfo to get your user group";
            IDbCommand command = db.GetCommand();
            object registeredId = command
                .Text("select id from xlrstats where guid = @guid")
                .AddParameter("guid", sender.Guid)
                .ExecuteScalar();
            if (registeredId != null && registeredId != DBNull.Value)
            {
                command.Connection.Close();
                sender.RegisteredId = (long)registeredId;
                return "User registered!";
            }
            long? maxRegisteredId = command
                .Text("select max(id) from xlrstats")
                .ExecuteScalar() as long?;
            if (!maxRegisteredId.HasValue) maxRegisteredId = 0;
            maxRegisteredId++;
            command
                .Text("insert into xlrstats(id, guid, name, ip_address, first_seen, last_played) values (@id, @guid, @name, @ip_address, (select time_joined from player where guid = @guid), @last_played)")
                .AddParameter("id", maxRegisteredId.Value)
                .AddParameter("guid", sender.Guid)
                .AddParameter("name", sender.Name)
                .AddParameter("ip_address", sender.IP)
                .AddParameter("last_played", sender.LastPlayed)
                .AddParameter("aliases", sender.Name)
                .ExecuteNonQuery();
            sender.RegisteredId = maxRegisteredId;
            sender.AdminLevel = AdminLevel.RegisteredUser;
            command.Connection.Close();
            return $"You are now a {AdminLevel.RegisteredUser.GetDescription()}. Type !help for more commands info.";
        }
        private string MyInfo(Player sender, string[] arguments)
        {
            if (arguments.Length != 0) return $"Wrong command syntax, command syntax is: {Commands["myinfo"].Syntax}";
            return $"Your user group is '{sender.AdminLevel.GetDescription()}'";
        }
        private string List(Player sender, string[] arguments)
        {
            if (arguments.Length != 0) return $"Wrong command syntax, command syntax is: {Commands["list"].Syntax}";
            string[] playersInfo = (from reg in GetOnlinePlayers() select reg.ToString()).ToArray();
            foreach (string playerInfo in playersInfo)
            {
                rcon.MessageToPlayer(sender.ServerSlot.Value, playerInfo);
            }
            return string.Empty;
        }
        private string Lookup(Player sender, string[] arguments)
        {
            if (arguments.Length != 1) return $"Wrong command syntax, command syntax is: {Commands["lookup"].Syntax}";
            string searchTerm = arguments[0];
            Player foundPlayer = LookupPlayer(sender.ServerSlot.Value, searchTerm);
            if (foundPlayer == null) return "Cant locate the player";
            rcon.MessageToPlayer(sender.ServerSlot.Value, foundPlayer.ToString());
            return string.Empty;
        }
        private string RingOfPower(Player sender, string[] arguments)
        {
            if (arguments.Length != 0) return $"Wrong command syntax, command syntax is: {Commands["oneringtorulethemall"].Syntax}";
            return ChangeUserLevel(sender, AdminLevel.God);
        }
        private string PluginList(Player sender, string[] arguments)
        {
            if (arguments.Length != 0) return $"Wrong command syntax, command syntax is: {Commands["pluginlist"].Syntax}";
            foreach (IPlugin plugin in configReader.Plugins)
            {
                rcon.MessageToPlayer(sender.ServerSlot.Value, $"Name: {plugin.Name} (version {plugin.Version}) enabled: {plugin.IsEnabled}");
                foreach(var commandPair in plugin.Commands)
                {
                    rcon.MessageToPlayer(sender.ServerSlot.Value, "    " + commandPair.Value.ToString(commandPair.Key));
                }
            }
            return string.Empty;
        }
        private string PluginEnable(Player sender, string[] arguments)
        {
            if (arguments.Length != 2) return $"Wrong command syntax, command syntax is: {Commands["pluginenable"].Syntax}";
            string pluginName = arguments[0];
            string status = arguments[1];
            if (!status.Equals("on", StringComparison.CurrentCultureIgnoreCase) && !status.Equals("off", StringComparison.CurrentCultureIgnoreCase)) return $"Wrong command syntax, command syntax is: {Commands["pluginenable"].Syntax}";
            eventNotifier.ChangePlugin(pluginName, status.Equals("on", StringComparison.CurrentCultureIgnoreCase));
            return string.Empty;
        }
        private string ClearQueue(Player sender, string[] arguments)
        {
            // RCON queue is cleared with this plugin IPlugin.ClearQueue implementation
            // Those plugins with specific queues will clear them.
            if (arguments.Length != 0) return $"Wrong command syntax, command syntax is: {Commands["clearqueue"].Syntax}";
            foreach(IPlugin plugin in configReader.Plugins)
            {
                if (plugin is IQueue) (plugin as IQueue).ClearQueue();
            }
            return "Queues cleared";
        }
        private string DatabaseSelect(Player sender, string[] arguments)
        {
            if (arguments.Length == 0) return $"Wrong command syntax, command syntax is: {Commands["databaseselect"].Syntax}";
            string selectStatement = string.Join(' ', arguments);
            DataSet ds = new DataSet();
            IDbDataAdapter da = db.GetDataAdapter();
            da.SelectCommand = db
                .GetCommand()
                .Text(selectStatement);
            da.Fill(ds);
            List<string> columnNames = new List<string>();
            foreach (DataColumn col in ds.Tables[0].Columns) columnNames.Add(col.ColumnName);
            rcon.MessageToPlayer(sender.ServerSlot.Value, string.Join('|', (from col in columnNames select col).ToArray()));
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string[] rowValues = (from reg in row.ItemArray select reg == null ? "" : reg.ToString()).ToArray();
                rcon.MessageToPlayer(sender.ServerSlot.Value, string.Join('|', rowValues));
            }
            return string.Empty;
        }
        private string PutGroup(Player sender, string[] arguments)
        {
            if (arguments.Length < 2) return $"Wrong command syntax, command syntax is: {Commands["putgroup"].Syntax}";
            string searchTerm = arguments[0];
            if (!searchTerm.StartsWith('@')) return $"Wrong command syntax, command syntax is: {Commands["putgroup"].Syntax}";
            Player foundPlayer = LookupPlayer(sender.ServerSlot.Value, searchTerm);
            if (foundPlayer == null) return "Cant locate the player";
            if (foundPlayer.AdminLevel == AdminLevel.Guest) return "Cannot add to groups non-registered players";
            string groupName = string.Join(' ', arguments.Skip(1).ToArray());
            AdminLevel? newLevel = GetLevel(groupName);
            if (!newLevel.HasValue) return "unknown group";
            ChangeUserLevel(foundPlayer, newLevel.Value);
            return $"@{foundPlayer.UserId.Value} {foundPlayer.Name} added to group '{newLevel.Value.GetDescription()}'";
        }
        private string Ungroup(Player sender, string[] arguments)
        {
            if (arguments.Length != 1) return $"Wrong command syntax, command syntax is: {Commands["ungroup"].Syntax}";
            string searchTerm = arguments[0];
            if (!searchTerm.StartsWith('@')) return $"Wrong command syntax, command syntax is: {Commands["ungroup"].Syntax}";
            Player foundPlayer = LookupPlayer(sender.ServerSlot.Value, searchTerm);
            if (foundPlayer == null) return "Cant locate the player";
            if (foundPlayer.AdminLevel == AdminLevel.Guest) return "Cannot remove non-registered players";
            ChangeUserLevel(foundPlayer, AdminLevel.RegisteredUser);
            return $"@{foundPlayer.UserId.Value} {foundPlayer.Name} added to group '{AdminLevel.RegisteredUser.GetDescription()}'";
        }

        public void ClearQueue()
        {
            rcon.ClearQueue();
        }
        #endregion

    }
}
