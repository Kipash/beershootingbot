﻿using BeerShootingBot.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BeerShootingBot.PlayerWatcher
{
    public class PlayerWatcher : PluginBase, IQueue
    {
        public override string Name => "PlayerWatcher";

        public override string Version => "0.1";

        protected override Dictionary<string, Command> GetCommands() => new Dictionary<string, Command>()
        {
            {
                "locate",
                new Command()
                {
                    Description = "Get the location information of a player. Once done you will get a private message",
                    Syntax = "location playerName|playerAuth|slot|@registeredId",
                    Execute = LocatePlayer
                }
            },
            {
                "vpn",
                new Command()
                {
                    Description = "Checks if a players is under a VPN or proxy. Once done you will get a private message",
                    Syntax = "vpn playerName|playerAuth|slot|@registeredId",
                    Execute = CheckVPN
                }
            }
        };

        private IJsonService<GeolocationResults> geolocationService;
        private IJsonService<bool> vpnService;
        private Dictionary<ServiceSubscription, List<string>> geolocationSubscriptions = new Dictionary<ServiceSubscription, List<string>>();
        private Dictionary<ServiceSubscription, List<string>> vpnSubscriptions = new Dictionary<ServiceSubscription, List<string>>();
        private List<PlayerLocation> locationsChecked = new List<PlayerLocation>();

        public PlayerWatcher(IConfigurationReader configReader, IEventNotifier eventNotifier, IRconManager rconManager, IDatabaseProvider databaseProvider, IJsonService<GeolocationResults> geolocationService, IJsonService<bool> vpnService) : base(configReader, eventNotifier, rconManager, databaseProvider)
        {
            this.geolocationService = geolocationService;
            this.vpnService = vpnService;

            eventNotifier.PlayerEntered += EventNotifier_PlayerEntered;
            eventNotifier.PluginChanged += EventNotifier_PluginChanged;

            // User commands event subscriptions
            this.geolocationService.DataRetrieved += GeolocationService_DataRetrieved;
            this.vpnService.DataRetrieved += VpnService_DataRetrieved;
            // Suspicious player event subscriptions
            this.geolocationService.DataRetrieved += GeolocationService_InternalDataRetrieved;
            this.vpnService.DataRetrieved += VpnService_InternalDataRetrieved;
        }

        private void EventNotifier_PluginChanged(string pluginName, bool enabled)
        {
            if (!Name.Equals(pluginName, StringComparison.CurrentCultureIgnoreCase)) return;
            currentEnabled = enabled;
        }

        private void VpnService_DataRetrieved(string address, bool results)
        {
            ServiceSubscription subscription = vpnSubscriptions.Keys.SingleOrDefault(p => p.IP == address);
            if (subscription.Equals(default(ServiceSubscription))) return;
            foreach (string guid in vpnSubscriptions[subscription])
            {
                Player playerToNotify = configReader.Game.SearchPlayerByGuid(guid);
                if (playerToNotify != null)
                {
                    rcon.MessageToPlayer(playerToNotify.ServerSlot.Value, $"Player:{subscription.Nick}.VPN or proxy used:{results}");
                }
            }
            vpnSubscriptions.Remove(subscription);
        }

        private void GeolocationService_DataRetrieved(string address, GeolocationResults results)
        {
            ServiceSubscription subscription = geolocationSubscriptions.Keys.SingleOrDefault(p => p.IP == address);
            if (subscription.Equals(default(ServiceSubscription))) return;
            foreach (string guid in geolocationSubscriptions[subscription])
            {
                Player playerToNotify = configReader.Game.SearchPlayerByGuid(guid);
                if (playerToNotify != null)
                {
                    rcon.MessageToPlayer(playerToNotify.ServerSlot.Value, $"Player: {subscription.Nick} Country:{results.countryName} Region:{results.regionName} Zip code:{results.zipCode}");
                }
            }
            geolocationSubscriptions.Remove(subscription);
        }

        private void VpnService_InternalDataRetrieved(string address, bool results)
        {
            var playersFromThatIP = GetOnlinePlayers(p => p.IP == address && !p.UsingProxyOrVPN.HasValue);
            foreach(Player player in playersFromThatIP) player.UsingProxyOrVPN = results;
            if (results)
            {
                string message = $"Players using proxy or VPN: " + string.Join(' ', (from player in playersFromThatIP select $"{player.Name}(slot {player.ServerSlot.Value}, Id {player.UserId.Value})").ToArray());
                MassivePrivateMessage(AdminLevel.Moderator, message);
            }
        }

        private void GeolocationService_InternalDataRetrieved(string address, GeolocationResults results)
        {
            var playersFromThatIP = GetOnlinePlayers(p => p.IP == address && string.IsNullOrEmpty(p.Country) && p.FollowedReason == null);
            foreach (Player player in playersFromThatIP)
            {
                player.Country = results.countryName;
                player.City = results.cityName;
            }
            PlayerLocation location = new PlayerLocation() { City = results.cityName, Country = results.countryName };
            if (locationsChecked.Contains(location)) return;
            IDbCommand command = db.GetCommand()
                .Text("select reason from followed_locations where city = @city and country = @country")
                .AddParameter("city", location.City)
                .AddParameter("country", location.Country);
            object followReason = command.ExecuteScalar();
            command.Connection.Close();
            if (followReason != null && followReason != DBNull.Value)
            {
                foreach (Player player in playersFromThatIP) player.FollowedReason = (string)followReason;
                string message = $"Players from a followed location (reason is '{followReason}'): " + string.Join(' ', (from player in playersFromThatIP select $"{player.Name}(slot {player.ServerSlot.Value}, Id {player.UserId.Value})").ToArray());
                MassivePrivateMessage(AdminLevel.Moderator, message);
            }
        }

        private void EventNotifier_PlayerEntered(Player newPlayer)
        {
            if (!newPlayer.IsBot)
            {
                // Background check for players IP to trigger suspicious players (proxy/VPN/followed locations)
                geolocationService.Enqueue(newPlayer.IP);
                vpnService.Enqueue(newPlayer.IP);
            }
        }

        protected override bool GetEnabled(AppConfiguration config)
        {
            return config.PlayerWatcherEnabled.Value;
        }

        private string CheckVPN(Player sender, string[] arguments)
        {
            if (arguments.Length != 1) return $"Wrong command syntax, command syntax is: {Commands["location"].Syntax}";
            Player playerToLocate = configReader.Game.SearchPlayer(arguments[0]);
            if (playerToLocate == null) return $"Don't know who is player '{arguments[0]}'. Try using slot";
            ServiceSubscription subscription = vpnSubscriptions.Keys.SingleOrDefault(p => p.IP == playerToLocate.IP);
            if (subscription.Equals(default(ServiceSubscription)))
            {
                vpnSubscriptions.Add(new ServiceSubscription() { IP = playerToLocate.IP, Nick = playerToLocate.Name }, new List<string>() { sender.Guid });
            }
            else
            {
                if (!vpnSubscriptions[subscription].Contains(sender.Guid)) vpnSubscriptions[subscription].Add(sender.Guid);
            }
            vpnService.Enqueue(playerToLocate.IP);
            return $"Checking for VPN/proxy usage for player {playerToLocate.Name}, this can take long, you will get a private message when check id done";
        }

        private string LocatePlayer(Player sender, string[] arguments)
        {
            if (arguments.Length != 1) return $"Wrong command syntax, command syntax is: {Commands["location"].Syntax}";
            Player playerToLocate = configReader.Game.SearchPlayer(arguments[0]);
            if (playerToLocate == null) return $"Don't know who is player '{arguments[0]}'. Try using slot";
            ServiceSubscription subscription = geolocationSubscriptions.Keys.SingleOrDefault(p => p.IP == playerToLocate.IP);
            if (subscription.Equals(default(ServiceSubscription)))
            {
                geolocationSubscriptions.Add(new ServiceSubscription() { IP = playerToLocate.IP, Nick = playerToLocate.Name }, new List<string>() { sender.Guid });
            }
            else
            {
                if (!geolocationSubscriptions[subscription].Contains(sender.Guid)) geolocationSubscriptions[subscription].Add(sender.Guid);
            }
            geolocationService.Enqueue(playerToLocate.IP);
            return "Checking for the player location, you will get a private message when check id done";
        }

        public void ClearQueue()
        {
            vpnService.ClearQueue();
            geolocationService.ClearQueue();
        }

        /// <summary>
        /// Struct used to enqueue and later notify player info lookups (ip to lookup and the player nick to the feedback to the admin)
        /// </summary>
        internal struct ServiceSubscription
        {
            internal string IP;
            internal string Nick;
        }

        internal struct PlayerLocation
        {
            internal string City;
            internal string Country;
        }
    }
}
