﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerShootingBot.Core
{
    public class ConsoleLogger : BeerShootingBot.Common.ILog
    {
        public void WriteLine(string msg, BeerShootingBot.Common.LogImportance importance = Common.LogImportance.Normal)
        {
            switch(importance)
            {
                case Common.LogImportance.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case Common.LogImportance.Success:
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
                case Common.LogImportance.Info:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case Common.LogImportance.Normal:
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    break;
            }

            Console.WriteLine(msg);

            Console.ForegroundColor = ConsoleColor.DarkGray;
        }
    }
}
