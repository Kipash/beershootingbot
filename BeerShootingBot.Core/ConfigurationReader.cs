﻿using BeerShootingBot.Common;
using System.Configuration;
using System.IO;

namespace BeerShootingBot.Core
{
    public class ConfigurationReader : IConfigurationReader
    {
        public IPlugin[] Plugins { get; set; }
        public GameStatus Game { get; set; }

        private AppConfiguration config = null;

        public AppConfiguration ReadConfiguration()
        {
            if (config != null) return config;
            var defaultConfig = new FileInfo("config.default.json").AsJson<AppConfiguration>();

            // user/enviroment configuration
            FileInfo userInfo = new FileInfo("config.user.json");
            if (userInfo.Exists)
            {
                var userConfig = userInfo.AsJson<AppConfiguration>();
                if (userConfig.BalancingEnabled.HasValue) defaultConfig.BalancingEnabled = userConfig.BalancingEnabled;
                if (userConfig.CommandLevels != null) defaultConfig.CommandLevels = userConfig.CommandLevels;
                if (userConfig.Database != null) defaultConfig.Database = userConfig.Database;
                if (!string.IsNullOrWhiteSpace(userConfig.GeolocationApiKey)) defaultConfig.GeolocationApiKey = userConfig.GeolocationApiKey;
                if (!string.IsNullOrWhiteSpace(userConfig.LogFile)) defaultConfig.LogFile = userConfig.LogFile;
                if (userConfig.Rcon != null) defaultConfig.Rcon = userConfig.Rcon;
            }
            config = defaultConfig;
            return config;
        }
    }
}
