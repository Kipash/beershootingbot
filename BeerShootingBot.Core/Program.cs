﻿using BeerShootingBot.Common;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;

namespace BeerShootingBot.Core
{
    class Program
    {
        internal static ServiceProvider Services;

        private static ServiceProvider ConfigureDependencyInjection()
        {
            var defaultConfig = new FileInfo("config.default.json").AsJson<AppConfiguration>();
            
            // user/enviroment configuration
            FileInfo userInfo = new FileInfo("config.user.json");
            if (userInfo.Exists)
            {
                var userConfig = userInfo.AsJson<AppConfiguration>();

                foreach (AppConfiguration.Component component in userConfig.Components)
                {
                    for (int i = 0; i < defaultConfig.Components.Length; i++)
                    {
                        if (defaultConfig.Components[i].Interface == component.Interface)
                        {
                            string realType = defaultConfig.Components[i].Type;
                            defaultConfig.Components[i] = component;
                            defaultConfig.Components[i].Type = realType;
                        }
                    }
                }

                defaultConfig.OverrideData(userConfig);
            }
            else
                Console.WriteLine("User has no configuration!");

            IServiceCollection collection = new ServiceCollection();

            // Different types require different registration functions
            Dictionary<string, Func<Type, Type, IServiceCollection>> functions = new Dictionary<string, Func<Type, Type, IServiceCollection>>()
            {
                { "Singleton", collection.AddSingleton },
                { "Scoped", collection.AddScoped  },
                { "Transient", collection.AddTransient  }
            };


            void addServices(AppConfiguration configuration, Dictionary<string, Func<Type, Type, IServiceCollection>> addFunctions)
            {
                foreach (AppConfiguration.Component component in configuration.Components)
                {
                    Func<Type, Type, IServiceCollection> function = addFunctions[component.Type];
                    Type interfaceType = GetType(component.Interface);
                    Type implementationType = GetType(component.Implementation);
                    if (interfaceType != null && implementationType != null) function(interfaceType, implementationType);
                }
            }

            addServices(defaultConfig, functions);

            return collection.BuildServiceProvider();
        }

        private static Type GetType(string name)
        {
            if(!name.Contains('[')) return Type.GetType(name);
            string genericName = name.Substring(0, name.IndexOf('[')) + name.Substring(name.LastIndexOf(']')+1);
            Type generic = Type.GetType(genericName);
            string subtypeName = name.Substring(name.IndexOf('[') + 1, name.Length - genericName.Length - 2);
            Type subtype = Type.GetType(subtypeName);
            Type returned = generic.MakeGenericType(subtype);
            return returned;
        }

        static void Main(string[] args)
        {
            BeerBotIntro();

            Services = ConfigureDependencyInjection();

            var logger = Services.GetService<ILog>();
            
            IConfigurationReader configReader = Services.GetService<IConfigurationReader>();
            IDatabaseProvider databaseProvider = Services.GetService<IDatabaseProvider>();

            // Check database structure before all classes constructors are called
            CheckDatabaseStructure(databaseProvider, configReader);
            
            IEventNotifier eventNotifier = Services.GetService<IEventNotifier>();
            IRconManager rcon = Services.GetService<IRconManager>();
            IJsonService<GeolocationResults> geolocation = Services.GetService<IJsonService<GeolocationResults>>();
            IJsonService<bool> vpnDetection = Services.GetService<IJsonService<bool>>();

            configReader.Plugins = new IPlugin[]
            {
                new BeerShootingBot.Administration.Administration(configReader, eventNotifier, rcon, databaseProvider),
                new BeerShootingBot.Balancing.Balancing(configReader, eventNotifier, rcon, databaseProvider),
                new BeerShootingBot.PlayerWatcher.PlayerWatcher(configReader, eventNotifier, rcon, databaseProvider, geolocation, vpnDetection)
            };

            AppConfiguration config = configReader.ReadConfiguration();

            // Set each command to the configured level
            Dictionary<string, AdminLevel> configuredLevels = new Dictionary<string, AdminLevel>();
            foreach (string commandName in config.CommandLevels.RegisteredUser) configuredLevels.Add(commandName, AdminLevel.RegisteredUser);
            foreach (string commandName in config.CommandLevels.Guest) configuredLevels.Add(commandName, AdminLevel.Guest);
            foreach (string commandName in config.CommandLevels.Moderator) configuredLevels.Add(commandName, AdminLevel.Moderator);
            foreach (string commandName in config.CommandLevels.FriendAdmin) configuredLevels.Add(commandName, AdminLevel.FriendAdmin);
            foreach (string commandName in config.CommandLevels.Member) configuredLevels.Add(commandName, AdminLevel.Member);
            foreach (string commandName in config.CommandLevels.BotAdmin) configuredLevels.Add(commandName, AdminLevel.BotAdmin);
            foreach (string commandName in config.CommandLevels.God) configuredLevels.Add(commandName, AdminLevel.God);
            foreach(IPlugin plugin in configReader.Plugins)
            {
                foreach (var commandPair in plugin.Commands)
                {
                    commandPair.Value.MinimunLevel = configuredLevels[commandPair.Key];
                }
            }

            // Now that all plugins are hooked into, read initial game status
            eventNotifier.ReadInitialStatus();

            logger.WriteLine("Initialized - Waiting for new events", LogImportance.Info);

            while(true)
            {

            }
        }


        private static void CheckDatabaseStructure(IDatabaseProvider provider, IConfigurationReader configReader)
        {
            string[] databaseCommandTexts = provider.GetDatabaseCreationScript();
            AppConfiguration.AppConfigurationDatabase dbSettings = configReader.ReadConfiguration().Database;
            DataBase db = new DataBase(dbSettings.ProviderName, dbSettings.ConnectionString);
            System.Data.IDbCommand command = db.GetCommand();
            foreach (string databaseCommandText in databaseCommandTexts)
            {
                command
                    .Text(databaseCommandText)
                    .ExecuteNonQuery();
            }
        }

        static void BeerBotIntro()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            Console.Clear();

            BeerGlass();
            
            Console.Title = "Beer bot";
        }
        static void BeerGlass()
        {
            Console.WriteLine(@"
        .~~~~.
       i ==== i_
       | cccc | _)
       | cccc |
       `-====-'");

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.DarkYellow;

            Console.WriteLine("\nBeer bot by BST family");

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("=======================");
            Console.WriteLine("\n\n\n");

            // .~~~~.
            //i ==== i_
            //| cccc | _)
            //| cccc |
            //`-====-'
        }
    }
}
